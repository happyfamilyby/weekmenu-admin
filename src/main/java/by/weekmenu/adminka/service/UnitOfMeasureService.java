package by.weekmenu.adminka.service;

import by.weekmenu.adminka.DTO.UnitOfMeasureDTO;

public interface UnitOfMeasureService extends CrudServiceClient<UnitOfMeasureDTO, Long> {
    Integer checkUniqueShortName(String shortName);
    Integer checkUniqueFullName(String fullName);
}
