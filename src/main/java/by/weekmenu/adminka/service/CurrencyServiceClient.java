package by.weekmenu.adminka.service;

import by.weekmenu.adminka.DTO.CurrencyDTO;
import by.weekmenu.adminka.ui.util.AppConsts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class CurrencyServiceClient implements CurrencyService {

    private final String currencyUrl;

    private final RestTemplate restTemplate;

    @Autowired
    public CurrencyServiceClient(RestTemplate restTemplate, @Value("${weekmenu.server.url}") String baseUrl) {
        this.restTemplate = restTemplate;
        currencyUrl = baseUrl + AppConsts.PAGE_CURRENCY;
    }

    @Override
    public RestTemplate getRestTemplate() {
        return restTemplate;
    }

    @Override
    public List<CurrencyDTO> getAllDTOs() {
        ResponseEntity<List<CurrencyDTO>> response = restTemplate.exchange(currencyUrl,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<CurrencyDTO>>() {});
        return response.getBody();
    }

    @Override
    public CurrencyDTO createNewDTO() {
        return new CurrencyDTO();
    }

    @Override
    public CurrencyDTO createDTO(CurrencyDTO entityDTO) {
        return restTemplate.postForObject(currencyUrl, entityDTO, CurrencyDTO.class);
    }

    @Override
    public void updateDTO(CurrencyDTO updatedDTO) {
        restTemplate.put(String.join("/", currencyUrl, updatedDTO.getId().toString()), updatedDTO);
    }

    @Override
    public void deleteDTO(CurrencyDTO entityDTO) {
        moveToRecycleBin(currencyUrl, entityDTO.getId(), entityDTO.getName());
    }

    @Override
    public Integer checkUniqueName(String name) {
        return restTemplate.getForObject(String.join("/", currencyUrl, "checkCurrencyUniqueName?name={name}")
                , Integer.class
                , name);
    }

    @Override
    public Integer checkUniqueCode(String code) {
        return restTemplate.getForObject(String.join("/", currencyUrl, "checkCurrencyUniqueCode?code={code}")
                , Integer.class
                , code);
    }
}
