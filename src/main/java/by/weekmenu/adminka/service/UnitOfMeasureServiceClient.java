package by.weekmenu.adminka.service;

import by.weekmenu.adminka.DTO.UnitOfMeasureDTO;
import by.weekmenu.adminka.ui.util.AppConsts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class UnitOfMeasureServiceClient implements UnitOfMeasureService {

    private final String urlUOM;

    private final RestTemplate restTemplate;

    @Autowired
    public UnitOfMeasureServiceClient(RestTemplate restTemplate, @Value("${weekmenu.server.url}") String baseUrl) {
        this.restTemplate = restTemplate;
        urlUOM = baseUrl + AppConsts.PAGE_UOM;
    }

    @Override
    public RestTemplate getRestTemplate() {
        return restTemplate;
    }

    @Override
    public List<UnitOfMeasureDTO> getAllDTOs() {
        return restTemplate.exchange(urlUOM, HttpMethod.GET, null,
                new ParameterizedTypeReference<List<UnitOfMeasureDTO>>() {})
                .getBody();
    }

    @Override
    public UnitOfMeasureDTO createNewDTO() {
        return new UnitOfMeasureDTO();
    }

    @Override
    public UnitOfMeasureDTO createDTO(UnitOfMeasureDTO entityDTO) {
        return restTemplate.postForObject(urlUOM, entityDTO, UnitOfMeasureDTO.class);
    }

    @Override
    public void updateDTO(UnitOfMeasureDTO updatedDTO) {
        String url = urlUOM + "/" + updatedDTO.getId();
        restTemplate.put(url, updatedDTO);
    }

    @Override
    public void deleteDTO(UnitOfMeasureDTO entityDTO) {
        moveToRecycleBin(urlUOM, entityDTO.getId(), entityDTO.getFullName());
    }

    @Override
    public Integer checkUniqueShortName(String shortName) {
        String url = urlUOM + "/" + "checkUniqueShortName?shortName={shortName}";
        return restTemplate.getForObject(url, Integer.class, shortName);
    }

    @Override
    public Integer checkUniqueFullName(String fullName) {
        String url = urlUOM + "/" + "checkUniqueFullName?fullName={fullName}";
        return restTemplate.getForObject(url, Integer.class, fullName);
    }
}
