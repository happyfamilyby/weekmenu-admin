package by.weekmenu.adminka.service;

import by.weekmenu.adminka.DTO.RecipeDTO;
import by.weekmenu.adminka.ui.util.AppConsts;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class RecipeServiceClient implements RecipeService {

    private final String urlRecipes;
    private final RestTemplate restTemplate;

    public RecipeServiceClient(RestTemplate restTemplate,
                               @Value("${weekmenu.server.url}") String baseUrl) {
        this.urlRecipes = baseUrl + AppConsts.PAGE_RECIPE;
        this.restTemplate = restTemplate;
    }

    @Override
    public RestTemplate getRestTemplate() {
        return restTemplate;
    }

    @Override
    public List<RecipeDTO> getAllDTOs() {
        return restTemplate.exchange(urlRecipes, HttpMethod.GET, null,
                new ParameterizedTypeReference<List<RecipeDTO>>() {})
                .getBody();
    }

    @Override
    public RecipeDTO createNewDTO() {
        return new RecipeDTO();
    }

    @Override
    public RecipeDTO createDTO(RecipeDTO entityDTO) {
        //TODO DELETE after adding cooking method
        entityDTO.setCookingMethodName("Варка");
            ResponseEntity<RecipeDTO> exchange =
                restTemplate.exchange(urlRecipes, HttpMethod.POST, new HttpEntity<>(entityDTO), RecipeDTO.class);
        return exchange.getBody();
    }

    @Override
    public void updateDTO(RecipeDTO updatedDTO) {
        String url = urlRecipes + "/" + updatedDTO.getId();
        restTemplate.exchange(url, HttpMethod.PUT, new HttpEntity<>(updatedDTO), Void.class);
    }

    @Override
    public void deleteDTO(RecipeDTO entityDTO) {
        moveToRecycleBin(urlRecipes, entityDTO.getId(), entityDTO.getName());
    }

    @Override
    public Integer checkUniqueName(String name) {
        String url = urlRecipes+ "/" + "checkUniqueName?name={name}";
        return restTemplate.exchange(url, HttpMethod.GET, null, Integer.class, name).getBody();
    }
}
