package by.weekmenu.adminka.service;

import by.weekmenu.adminka.DTO.CookingMethodDTO;
import by.weekmenu.adminka.ui.util.AppConsts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class CookingMethodServiceClient implements CookingMethodService {

        private final String urlCookingMethods;
        private final RestTemplate restTemplate;

        @Autowired
        public CookingMethodServiceClient(RestTemplate restTemplate, @Value("${weekmenu.server.url}") String baseUrl) {
            this.restTemplate = restTemplate;
            urlCookingMethods = baseUrl + AppConsts.PAGE_COOKINGMETHODS;
        }

    @Override
    public RestTemplate getRestTemplate() {
        return restTemplate;
    }

    @Override
        public List<CookingMethodDTO> getAllDTOs() {
            return restTemplate.exchange(urlCookingMethods, HttpMethod.GET, null,
                    new ParameterizedTypeReference<List<CookingMethodDTO>>() {}).getBody();
        }

        @Override
        public CookingMethodDTO createNewDTO() {
            return new CookingMethodDTO();
        }

        @Override
        public CookingMethodDTO createDTO(CookingMethodDTO entityDTO) {
            return restTemplate.postForObject(urlCookingMethods, entityDTO, CookingMethodDTO.class);
        }

        @Override
        public void updateDTO(CookingMethodDTO updatedDTO) {
            String url = urlCookingMethods + "/" + updatedDTO.getId();
            restTemplate.put(url, updatedDTO);

        }

        @Override
        public void deleteDTO(CookingMethodDTO entityDTO) {
            moveToRecycleBin(urlCookingMethods, entityDTO.getId(), entityDTO.getName());
        }

        @Override
        public Integer checkCookingMethodUniqueName(String name) {
            String url = urlCookingMethods + "/" + "checkCookingMethodUniqueName?name={name}";
            return restTemplate.getForObject(url, Integer.class, name);
        }
    }