package by.weekmenu.adminka.service;

import by.weekmenu.adminka.ui.components.CheckConnectedElementsNotification;
import com.vaadin.flow.component.notification.Notification;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Objects;

public interface CrudServiceClient<DTO, ID> {

    RestTemplate getRestTemplate();

    List<DTO> getAllDTOs();

//    List<DTO> searchName(String name);

    DTO createNewDTO();

    DTO createDTO(DTO entityDTO);

    void updateDTO(DTO updatedDTO);

    void deleteDTO(DTO entityDTO);

    default void moveToRecycleBin(String url, ID entityID, String entityName) {
        String checkUrl = url + "/checkConnectedElements/" + entityID;
        List<String> result = getRestTemplate().exchange(checkUrl, HttpMethod.GET, null,
                new ParameterizedTypeReference<List<String>>() {})
                .getBody();
        if (Objects.requireNonNull(result).size()==0) {
            getRestTemplate().delete(String.join("/", url, entityID.toString()));
            Notification.show(entityName + " перемещён в корзину");
        } else {
            new CheckConnectedElementsNotification(entityName, result);
        }
    }
}