package by.weekmenu.adminka.service;

import by.weekmenu.adminka.DTO.IngredientDTO;

import java.util.List;

public interface IngredientService extends CrudServiceClient<IngredientDTO, Long> {

    Integer checkUniqueName(String name);
    List<String> getAllUnitOfMeasures(String ingredientName);
}
