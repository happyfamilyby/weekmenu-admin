package by.weekmenu.adminka.service;

import by.weekmenu.adminka.DTO.CookingMethodDTO;

public interface CookingMethodService extends CrudServiceClient<CookingMethodDTO, Integer> {

    Integer checkCookingMethodUniqueName(String name);
}