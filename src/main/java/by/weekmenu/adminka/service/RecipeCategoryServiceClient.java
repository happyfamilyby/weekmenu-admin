package by.weekmenu.adminka.service;

import by.weekmenu.adminka.DTO.RecipeCategoryDTO;
import by.weekmenu.adminka.ui.util.AppConsts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class RecipeCategoryServiceClient implements RecipeCategoryService {

        private final String urlRecipeCategories;
        private final RestTemplate restTemplate;

        @Autowired
        public RecipeCategoryServiceClient(RestTemplate restTemplate, @Value("${weekmenu.server.url}") String baseUrl) {
            this.restTemplate = restTemplate;
            urlRecipeCategories = baseUrl + AppConsts.PAGE_RECIPECATEGORY;
        }

    @Override
    public RestTemplate getRestTemplate() {
        return restTemplate;
    }

    @Override
        public List<RecipeCategoryDTO> getAllDTOs() {
            return restTemplate.exchange(urlRecipeCategories, HttpMethod.GET, null,
                    new ParameterizedTypeReference<List<RecipeCategoryDTO>>() {}).getBody();
        }

        @Override
        public RecipeCategoryDTO createNewDTO() {
            return new RecipeCategoryDTO();
        }

        @Override
        public RecipeCategoryDTO createDTO(RecipeCategoryDTO entityDTO) {
            return restTemplate.postForObject(urlRecipeCategories, entityDTO, RecipeCategoryDTO.class);
        }

        @Override
        public void updateDTO(RecipeCategoryDTO updatedDTO) {
            String url = urlRecipeCategories + "/" + updatedDTO.getId();
            restTemplate.put(url, updatedDTO);

        }

        @Override
        public void deleteDTO(RecipeCategoryDTO entityDTO) {
            moveToRecycleBin(urlRecipeCategories, entityDTO.getId(), entityDTO.getName());
        }

        @Override
        public Integer checkRecipeCategoryUniqueName(String name) {
            String url = urlRecipeCategories + "/" + "checkRecipeCategoryUniqueName?name={name}";
            return restTemplate.getForObject(url, Integer.class, name);
        }
    }