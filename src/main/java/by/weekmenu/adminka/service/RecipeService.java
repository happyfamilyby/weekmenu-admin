package by.weekmenu.adminka.service;

import by.weekmenu.adminka.DTO.RecipeDTO;

public interface RecipeService extends CrudServiceClient<RecipeDTO, Long> {

    Integer checkUniqueName(String name);
}
