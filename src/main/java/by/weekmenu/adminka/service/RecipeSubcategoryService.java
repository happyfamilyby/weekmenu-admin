package by.weekmenu.adminka.service;

import by.weekmenu.adminka.DTO.RecipeSubcategoryDTO;

public interface RecipeSubcategoryService extends CrudServiceClient<RecipeSubcategoryDTO, Long> {

    Integer checkRecipeSubcategoryUniqueName(String name);
}