package by.weekmenu.adminka.service;

import by.weekmenu.adminka.DTO.RecipeCategoryDTO;

public interface RecipeCategoryService extends CrudServiceClient<RecipeCategoryDTO, Long> {

    Integer checkRecipeCategoryUniqueName(String name);
}