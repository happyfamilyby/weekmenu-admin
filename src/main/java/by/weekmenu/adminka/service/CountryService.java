package by.weekmenu.adminka.service;

import by.weekmenu.adminka.DTO.CountryDTO;

import java.util.List;

public interface CountryService extends CrudServiceClient<CountryDTO, Long>{

    Integer checkUniqueName(String name);
    Integer checkUniqueAlphaCode2(String alphaCode2);
    List<String> getAllCountryNames();
}
