package by.weekmenu.adminka.service;

import by.weekmenu.adminka.DTO.RecipeSubcategoryDTO;
import by.weekmenu.adminka.ui.util.AppConsts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class RecipeSubcategoryServiceClient implements RecipeSubcategoryService {

        private final String urlRecipeSubcategories;
        private final RestTemplate restTemplate;

        @Autowired
        public RecipeSubcategoryServiceClient(RestTemplate restTemplate, @Value("${weekmenu.server.url}") String baseUrl) {
            this.restTemplate = restTemplate;
            urlRecipeSubcategories = baseUrl + AppConsts.PAGE_RECIPESUBCATEGORY;
        }

    @Override
    public RestTemplate getRestTemplate() {
        return restTemplate;
    }

    @Override
        public List<RecipeSubcategoryDTO> getAllDTOs() {
           return restTemplate.exchange(urlRecipeSubcategories, HttpMethod.GET, null,
                   new ParameterizedTypeReference<List<RecipeSubcategoryDTO>>() {})
                   .getBody();
        }

        @Override
        public RecipeSubcategoryDTO createNewDTO() {
            return new RecipeSubcategoryDTO();
        }

        @Override
        public RecipeSubcategoryDTO createDTO(RecipeSubcategoryDTO entityDTO) {
            return restTemplate.postForObject(urlRecipeSubcategories, entityDTO, RecipeSubcategoryDTO.class);
        }

        @Override
        public void updateDTO(RecipeSubcategoryDTO updatedDTO) {
            String url = urlRecipeSubcategories + "/" + updatedDTO.getId();
            restTemplate.put(url, updatedDTO);

        }

        @Override
        public void deleteDTO(RecipeSubcategoryDTO entityDTO) {
            moveToRecycleBin(urlRecipeSubcategories, entityDTO.getId(), entityDTO.getName());
        }

        @Override
        public Integer checkRecipeSubcategoryUniqueName(String name) {
            String url = urlRecipeSubcategories + "/" + "checkRecipeSubcategoryUniqueName?name={name}";
            return restTemplate.getForObject(url, Integer.class, name);
        }
    }