package by.weekmenu.adminka.service;

import by.weekmenu.adminka.DTO.CountryDTO;
import by.weekmenu.adminka.ui.util.AppConsts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class CountryServiceClient implements CountryService {

    private final String urlCountries;

    private final RestTemplate restTemplate;

    @Autowired
    public CountryServiceClient(RestTemplate restTemplate, @Value("${weekmenu.server.url}") String baseUrl) {
        this.restTemplate = restTemplate;
        urlCountries = baseUrl + AppConsts.PAGE_COUNTRY;
    }

    @Override
    public RestTemplate getRestTemplate() {
        return restTemplate;
    }

    @Override
    public List<CountryDTO> getAllDTOs() {
        return restTemplate.exchange(urlCountries, HttpMethod.GET, null,
                new ParameterizedTypeReference<List<CountryDTO>>() {})
                .getBody();
    }

    @Override
    public CountryDTO createNewDTO() {
        return new CountryDTO();
    }

    @Override
    public CountryDTO createDTO(CountryDTO entityDTO) {
        return restTemplate.postForObject(urlCountries, entityDTO, CountryDTO.class);
    }

    @Override
    public void updateDTO(CountryDTO updatedDTO) {
        String url = urlCountries + "/" + updatedDTO.getId();
        restTemplate.put(url, updatedDTO);

    }

    @Override
    public void deleteDTO(CountryDTO entityDTO) {
        moveToRecycleBin(urlCountries, entityDTO.getId(), entityDTO.getName());
    }


    @Override
    public Integer checkUniqueName(String name) {
        String url = urlCountries + "/" + "checkUniqueName?name={name}";
        return restTemplate.getForObject(url, Integer.class, name);
    }

    @Override
    public Integer checkUniqueAlphaCode2(String alphaCode2) {
        String url = urlCountries + "/" + "checkUniqueAlphaCode2?alphaCode2={alphaCode2}";
        return restTemplate.getForObject(url, Integer.class, alphaCode2);
    }

    @Override
    public List<String> getAllCountryNames() {
        return restTemplate.exchange(urlCountries + "/" + "names",
                HttpMethod.GET, null,
                new ParameterizedTypeReference<List<String>>() {})
                .getBody();
    }
}
