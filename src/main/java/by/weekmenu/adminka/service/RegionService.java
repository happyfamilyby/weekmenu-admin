package by.weekmenu.adminka.service;

import by.weekmenu.adminka.DTO.RegionDTO;

public interface RegionService extends CrudServiceClient<RegionDTO, Long> {

    RegionDTO findRegionByName(String name);
    Integer checkUniqueName(String name);

}
