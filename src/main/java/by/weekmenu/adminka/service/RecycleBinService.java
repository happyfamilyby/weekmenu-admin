package by.weekmenu.adminka.service;

import by.weekmenu.adminka.DTO.RecycleBinDTO;

import java.util.List;

public interface RecycleBinService {

    List<RecycleBinDTO> getAllBins();
    void delete(Long id);
    void restore(Long id);
}
