package by.weekmenu.adminka.service;

import by.weekmenu.adminka.DTO.RecycleBinDTO;
import by.weekmenu.adminka.ui.util.AppConsts;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class RecycleBinServiceImpl implements RecycleBinService {

    private final String urlRecycleBin;
    private final RestTemplate restTemplate;

    public RecycleBinServiceImpl(RestTemplate restTemplate,
                                 @Value("${weekmenu.server.url}") String baseUrl) {
        this.urlRecycleBin = baseUrl + AppConsts.PAGE_RECYCLE_BIN;
        this.restTemplate = restTemplate;
    }

    @Override
    public List<RecycleBinDTO> getAllBins() {
        return restTemplate.exchange(urlRecycleBin, HttpMethod.GET, null,
                new ParameterizedTypeReference<List<RecycleBinDTO>>() {})
                .getBody();
    }

    @Override
    public void delete(Long id) {
        String url = urlRecycleBin + "/" + id;
        restTemplate.delete(url);
    }

    @Override
    public void restore(Long id) {
        String url = urlRecycleBin + "/" + id;
        restTemplate.exchange(url, HttpMethod.PUT, null, Void.class);
    }
}
