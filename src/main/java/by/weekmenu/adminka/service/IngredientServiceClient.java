package by.weekmenu.adminka.service;

import by.weekmenu.adminka.DTO.IngredientDTO;
import by.weekmenu.adminka.ui.util.AppConsts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class IngredientServiceClient implements IngredientService {

    private final String urlIngredients;

    private final RestTemplate restTemplate;

    @Autowired
    public IngredientServiceClient(RestTemplate restTemplate,
                                   @Value("${weekmenu.server.url}") String baseUrl) {
        this.restTemplate = restTemplate;
        urlIngredients = baseUrl + AppConsts.PAGE_INGREDIENT;
    }

    @Override
    public RestTemplate getRestTemplate() {
        return restTemplate;
    }

    @Override
    public List<IngredientDTO> getAllDTOs() {
        ResponseEntity<List<IngredientDTO>> response = restTemplate.exchange(urlIngredients, HttpMethod.GET, null,
                new ParameterizedTypeReference<List<IngredientDTO>>() {});
        return response.getBody();
    }

    @Override
    public IngredientDTO createNewDTO() {
        return new IngredientDTO();
    }

    @Override
    public IngredientDTO createDTO(IngredientDTO ingredientDTO) {
        return restTemplate.postForObject(urlIngredients, ingredientDTO, IngredientDTO.class);
    }

    public IngredientDTO getIngredientById(Long id) {
        return restTemplate.getForObject(urlIngredients + "/" + id, IngredientDTO.class);
    }

    @Override
    public void updateDTO(IngredientDTO updatedIngredientDTO) {
        String url = urlIngredients + "/" + updatedIngredientDTO.getId();
        restTemplate.put(url, updatedIngredientDTO);
    }

    @Override
    public void deleteDTO(IngredientDTO ingredientDTO) {
        moveToRecycleBin(urlIngredients, ingredientDTO.getId(), ingredientDTO.getName());
    }

    @Override
    public Integer checkUniqueName(String name) {
        String url = urlIngredients + "/" + "checkUniqueName?name={name}";
        return restTemplate.getForObject(url, Integer.class, name);
    }

    @Override
    public List<String> getAllUnitOfMeasures(String ingredientName) {
        String url = urlIngredients + "/" + "getUnitOfMeasures?name={name}";
        return restTemplate.exchange(url, HttpMethod.GET,
                null, new ParameterizedTypeReference<List<String>>() {}, ingredientName).getBody();
    }
}
