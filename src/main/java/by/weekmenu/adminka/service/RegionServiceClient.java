package by.weekmenu.adminka.service;

import by.weekmenu.adminka.DTO.RegionDTO;
import by.weekmenu.adminka.ui.util.AppConsts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service

public class RegionServiceClient implements  RegionService {

    private final String urlRegions;
    private final RestTemplate restTemplate;

    @Autowired
    public RegionServiceClient(RestTemplate restTemplate, @Value("${weekmenu.server.url}") String baseUrl) {
        this.restTemplate = restTemplate;
        urlRegions = baseUrl + AppConsts.PAGE_REGION;
    }

    @Override
    public RestTemplate getRestTemplate() {
        return restTemplate;
    }

    @Override
    public List<RegionDTO> getAllDTOs() {
        return restTemplate.exchange(urlRegions, HttpMethod.GET, null,
                new ParameterizedTypeReference<List<RegionDTO>>() {})
                .getBody();
    }

    @Override
    public RegionDTO createNewDTO() {
        return new RegionDTO();
    }

    @Override
    public RegionDTO createDTO(RegionDTO entityDTO) {
        return restTemplate.postForObject(urlRegions, entityDTO, RegionDTO.class);
    }

    @Override
    public void updateDTO(RegionDTO updatedDTO) {
        String url = urlRegions + "/" + updatedDTO.getId();
        restTemplate.put(url, updatedDTO);
    }

    @Override
    public void deleteDTO(RegionDTO entityDTO) {
        moveToRecycleBin(urlRegions, entityDTO.getId(), entityDTO.getName());
    }

    @Override
    public Integer checkUniqueName(String name) {
        String url = urlRegions + "/" + "checkUniqueName?name={name}";
        return restTemplate.getForObject(url, Integer.class, name);
    }

    @Override
    public RegionDTO findRegionByName(String name) {
        String url = urlRegions + "/name?name={name}";
        return restTemplate.getForObject(url, RegionDTO.class, name);
    }
}
