package by.weekmenu.adminka.DTO;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class RecipeIngredientDTO {

    private String ingredientName;
    private String quantity;
    private String unitOfMeasureShortName;
}