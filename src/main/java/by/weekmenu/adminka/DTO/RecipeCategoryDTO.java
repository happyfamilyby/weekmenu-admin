package by.weekmenu.adminka.DTO;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class RecipeCategoryDTO {

    private Long id;
    private String name;

}