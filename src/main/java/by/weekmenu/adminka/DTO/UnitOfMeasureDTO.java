package by.weekmenu.adminka.DTO;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@NoArgsConstructor
@Getter
@Setter
public class UnitOfMeasureDTO implements Serializable {

    private Long id;
    private String fullName;
    private String shortName;
}
