package by.weekmenu.adminka.DTO;


import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class RegionDTO {

    private Long id;
    private String name;
    private String countryName;
    private String countryCurrencyCode;
}
