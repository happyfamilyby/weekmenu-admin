package by.weekmenu.adminka.DTO;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

@NoArgsConstructor
@Getter
@Setter
public class RecipeDTO {

    private Long id;
    private String name;
    private String cookingTime;
    private String preparingTime;
    private BigDecimal calories;
    private BigDecimal proteins;
    private BigDecimal fats;
    private BigDecimal carbs;
    private String imageLink;
    private String source;
    private Double portions;
    private BigDecimal gramsPerPortion;
    private String ownershipName = "ADMIN";
    private String cookingMethodName;
    private Set<RecipeIngredientDTO> recipeIngredients;
    private List<CookingStepDTO> cookingSteps;
    private Set<RecipePriceDTO> recipePrices;
    private Set<String> categoryNames;
    private Set<String> subcategoryNames;
}