package by.weekmenu.adminka.DTO;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class CookingStepDTO {

    private Integer id;
    private String priority;
    private String description;
    private String imageLink;
}