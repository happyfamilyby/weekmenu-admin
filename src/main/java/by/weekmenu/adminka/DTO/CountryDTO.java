package by.weekmenu.adminka.DTO;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class CountryDTO {

    private Long id;
    private String name;
    private String alphaCode2;
    private String currencyCode;

    public void setAlphaCode2UpperCase(String code) {
        this.alphaCode2 = code.toUpperCase();
    }
}
