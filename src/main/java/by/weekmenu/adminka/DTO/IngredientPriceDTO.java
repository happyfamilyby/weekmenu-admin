package by.weekmenu.adminka.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@NoArgsConstructor
@Getter
@Setter
public class IngredientPriceDTO {

    private String regionName;
    @JsonProperty("unitOfMeasureName")
    private String unitOfMeasureFullName;
    private String quantityString;
    private String priceValueString;
    private BigDecimal quantity;
    private BigDecimal priceValue;
    private String currencyCode;
}
