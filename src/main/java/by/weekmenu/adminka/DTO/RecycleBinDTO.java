package by.weekmenu.adminka.DTO;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@NoArgsConstructor
@Getter
@Setter
public class RecycleBinDTO {

    private Long id;
    private String elementName;
    private String entityName;
    private LocalDateTime deleteDate;
}
