package by.weekmenu.adminka.DTO;

import by.weekmenu.adminka.ui.beans.IngredientUOM;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


@NoArgsConstructor
@Getter
@Setter
public class IngredientDTO implements Serializable {

    private Long id;
    private String name;
    private String caloriesString;
    private BigDecimal calories;
    private String proteinsString;
    private BigDecimal proteins;
    private String fatsString;
    private BigDecimal fats;
    private String carbsString;
    private BigDecimal carbs;

    @JsonProperty("unitOfMeasureEquivalent")
    private Map<String, BigDecimal> unitOfMeasureEquivalent;
    private List<IngredientUOM> ingredientUOMS;

    private Set<IngredientPriceDTO> ingredientPrices;

    public List<IngredientUOM> convertMapToList(Map<String, BigDecimal> unitOfMeasureEquivalent) {
        return unitOfMeasureEquivalent.entrySet().stream()
                .map(e -> new IngredientUOM(e.getKey(), e.getValue().toString().replace('.',',')))
                .collect(Collectors.toList());
    }

    public Map<String, BigDecimal> convertListToMap(List<IngredientUOM> ingredientUOMS) {
        return ingredientUOMS.stream()
                .collect(Collectors.toMap(IngredientUOM::getUnitOfMeasureFullName, ingredientUOM -> new BigDecimal(ingredientUOM.getEquivalent().replace(',','.'))));
    }
}
