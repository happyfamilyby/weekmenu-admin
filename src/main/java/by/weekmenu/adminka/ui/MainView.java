package by.weekmenu.adminka.ui;

import by.weekmenu.adminka.ui.components.AppNavigation;
import by.weekmenu.adminka.ui.components.PageInfo;
import by.weekmenu.adminka.ui.components.VerticalMenu;
import by.weekmenu.adminka.ui.util.AppConsts;
import by.weekmenu.adminka.ui.views.cookingMethod.CookingMethodView;
import by.weekmenu.adminka.ui.views.country.CountryView;
import by.weekmenu.adminka.ui.views.currency.CurrencyView;
import by.weekmenu.adminka.ui.views.ingredient.IngredientView;
import by.weekmenu.adminka.ui.views.recipe.RecipeView;
import by.weekmenu.adminka.ui.views.recipeCategory.RecipeCategoryView;
import by.weekmenu.adminka.ui.views.recipeCategory.RecipeSubcategoryView;
import by.weekmenu.adminka.ui.views.recycleBin.RecycleBinView;
import by.weekmenu.adminka.ui.views.region.RegionView;
import by.weekmenu.adminka.ui.views.unitOfMeasure.UnitOfMeasureView;
import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.HasElement;
import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.page.Viewport;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteAlias;
import com.vaadin.flow.router.RouterLayout;
import com.vaadin.flow.server.PWA;
import com.vaadin.flow.theme.Theme;
import com.vaadin.flow.theme.material.Material;

import java.util.ArrayList;
import java.util.List;

@Route
@RouteAlias(AppConsts.PAGE_MAIN)
@Theme(Material.class)
@PageTitle(AppConsts.TITLE_MAIN)
@Viewport(AppConsts.VIEWPORT)
@HtmlImport("frontend://custom-material-dialog.html")
@PWA(name = "Admin Panel for weekmenu.by", shortName = "Admin Panel")
public class MainView extends VerticalLayout implements HasComponents, RouterLayout {

    private final Div childWrapper = new Div();
    private final VerticalMenu verticalMenu;

    public MainView() {
        setSizeFull();
        List<PageInfo> pages = new ArrayList<>();
        pages.add(new PageInfo(AppConsts.PAGE_INGREDIENT,
                AppConsts.ICON_INGREDIENT, AppConsts.TITLE_INGREDIENT));
        pages.add(new PageInfo(AppConsts.PAGE_RECIPE,
                AppConsts.ICON_RECIPE, AppConsts.TITLE_RECIPE));
        pages.add(new PageInfo(AppConsts.PAGE_RECYCLE_BIN,
                AppConsts.ICON_RECYCLE_BIN, AppConsts.TITLE_RECYCLE_BIN));
        AppNavigation appNavigation = new AppNavigation();
        appNavigation.init(pages, AppConsts.PAGE_DEFAULT);
        add(appNavigation);
        setHorizontalComponentAlignment(Alignment.CENTER, appNavigation);

        HorizontalLayout mainContent = new HorizontalLayout();
        mainContent.setSizeFull();
        verticalMenu = new VerticalMenu();
        mainContent.add(verticalMenu, childWrapper);
        mainContent.setFlexGrow(1, childWrapper);
        setFlexGrow(1, mainContent);
        add(mainContent);
    }

    @Override
    public void showRouterLayoutContent(HasElement content) {
        childWrapper.getElement().appendChild(content.getElement());
        getVerticalMenu().removeAll();
        if (content instanceof IngredientView || content instanceof UnitOfMeasureView
        || content instanceof CurrencyView || content instanceof CountryView || content instanceof  RegionView) {
            List<PageInfo> links = new ArrayList<>();
            links.add(new PageInfo(AppConsts.PAGE_INGREDIENT,
                            AppConsts.ICON_INGREDIENT, AppConsts.TITLE_INGREDIENT, IngredientView.class));
            links.add(new PageInfo(AppConsts.PAGE_UOM,
                            AppConsts.ICON_UOM, AppConsts.TITLE_UOM, UnitOfMeasureView.class));
            links.add(new PageInfo(AppConsts.PAGE_CURRENCY,
                    AppConsts.ICON_CURRENCY, AppConsts.TITLE_CURRENCY, CurrencyView.class));
            links.add(new PageInfo(AppConsts.PAGE_COUNTRY,
                    AppConsts.ICON_COUNTRY, AppConsts.TITLE_COUNTRY, CountryView.class));
            links.add(new PageInfo(AppConsts.PAGE_REGION,
                    AppConsts.ICON_REGION, AppConsts.TITLE_REGION, RegionView.class));
            getVerticalMenu().initMenu(links);
        } else if (content instanceof RecipeView || content instanceof RecipeCategoryView ||
        content instanceof RecipeSubcategoryView || content instanceof CookingMethodView) {
            List<PageInfo> links = new ArrayList<>();
            links.add(new PageInfo(AppConsts.PAGE_RECIPE,
                    AppConsts.ICON_RECIPE, AppConsts.TITLE_RECIPE, RecipeView.class));
            links.add(new PageInfo(AppConsts.PAGE_RECIPECATEGORY,
                    AppConsts.ICON_RECIPECATEGORY, AppConsts.TITLE_RECIPECATEGORY, RecipeCategoryView.class));
            links.add(new PageInfo(AppConsts.PAGE_RECIPESUBCATEGORY,
                    AppConsts.ICON_RECIPESUBCATEGORY, AppConsts.TITLE_RECIPESUBCATEGORY, RecipeSubcategoryView.class));
            links.add(new PageInfo(AppConsts.PAGE_COOKINGMETHODS,
                    AppConsts.ICON_COOKINGMETHODS, AppConsts.TITLE_COOKINGMETHODS, CookingMethodView.class));
            getVerticalMenu().initMenu(links);
        } else if (content instanceof RecycleBinView) {
            List<PageInfo> links = new ArrayList<>();
            links.add(new PageInfo(AppConsts.PAGE_RECYCLE_BIN,
                    AppConsts.ICON_RECYCLE_BIN, AppConsts.TITLE_RECYCLE_BIN, RecycleBinView.class));
            getVerticalMenu().initMenu(links);
        }

    }

    private VerticalMenu getVerticalMenu() {
        return verticalMenu;
    }
}
