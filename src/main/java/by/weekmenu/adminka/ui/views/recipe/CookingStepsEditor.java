package by.weekmenu.adminka.ui.views.recipe;

import by.weekmenu.adminka.DTO.CookingStepDTO;
import com.vaadin.flow.component.AbstractField;
import com.vaadin.flow.component.HasValueAndElement;
import com.vaadin.flow.component.internal.AbstractFieldSupport;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.shared.Registration;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

class CookingStepsEditor extends VerticalLayout implements HasValueAndElement<AbstractField.ComponentValueChangeEvent<CookingStepsEditor, List<CookingStepDTO>>, List<CookingStepDTO>> {

    private final AbstractFieldSupport<CookingStepsEditor, List<CookingStepDTO>> fieldSupport;
    private final RecipeForm form;
    private final String uploadLocation;
    private final String pathPattern;
    
    public CookingStepsEditor(String uploadLocation,
                              String pathPattern,
                              RecipeForm form) {
        this.fieldSupport = new AbstractFieldSupport<>(this, Collections.emptyList(),
                Objects::equals, c ->  {});
        this.form = form;
        this.uploadLocation = uploadLocation;
        this.pathPattern = pathPattern;
        setSizeFull();
        setSpacing(false);
        setPadding(false);
    }

    @Override
    public void setValue(List<CookingStepDTO> cookingStepDTOS) {
        fieldSupport.setValue(cookingStepDTOS);
        removeAll();
        if (cookingStepDTOS !=null) {
            cookingStepDTOS.sort(Comparator.comparing(cookingStepDTO -> Integer.valueOf(cookingStepDTO.getPriority())));
            for (int i=0; i<cookingStepDTOS.size(); i++) {
                CookingStepEditor editor = createEditor(cookingStepDTOS.get(i));
                editor.getAddCookingStep().setVisible(false);
                if (i==cookingStepDTOS.size()-1) {
                    editor.getAddCookingStep().setVisible(true);
                }
            }
            if (cookingStepDTOS.size()==0) {
                createEditor(null);
            }
        }
    }
    
    CookingStepEditor createEditor(CookingStepDTO cookingStepDto) {
        CookingStepEditor editor = new CookingStepEditor(uploadLocation, pathPattern, form, cookingStepDto, this);
        getElement().appendChild(editor.getElement());
        editor.addCookingStepChangedEvent(e -> cookingStepChanged(e.getSource()));
        editor.setValue(cookingStepDto);
        if (cookingStepDto==null) {
            editor.getDeleteCookingStep().setVisible(false);
        }
        return editor;
    }

    private void cookingStepChanged(CookingStepEditor cookingStepEditor) {
        if(cookingStepEditor.getCookingStepDto()==null) {
            if (!cookingStepEditor.getDescription().isEmpty() && cookingStepEditor.getBinder().validate().isOk()) {
                cookingStepEditor.setCookingSteps();
            } else {
                cookingStepEditor.getBinder().validate();
            }
        }
    }

    @Override
    public List<CookingStepDTO> getValue() {
        return fieldSupport.getValue();
    }

    @Override
    public Registration addValueChangeListener(ValueChangeListener<? super AbstractField.ComponentValueChangeEvent<CookingStepsEditor, List<CookingStepDTO>>> valueChangeListener) {
        return fieldSupport.addValueChangeListener(valueChangeListener);
    }

    public void deleteCookingStep(CookingStepDTO cookingStepDto) {
        setValue(getValue().stream().filter(element -> element != cookingStepDto).collect(Collectors.toList()));
        form.getDTO().setCookingSteps(getValue());
    }
}
