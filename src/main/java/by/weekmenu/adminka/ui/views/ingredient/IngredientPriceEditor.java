package by.weekmenu.adminka.ui.views.ingredient;

import by.weekmenu.adminka.DTO.IngredientDTO;
import by.weekmenu.adminka.DTO.IngredientPriceDTO;
import by.weekmenu.adminka.DTO.RegionDTO;
import by.weekmenu.adminka.DTO.UnitOfMeasureDTO;
import by.weekmenu.adminka.service.RegionService;
import by.weekmenu.adminka.service.UnitOfMeasureService;
import by.weekmenu.adminka.ui.Events.DeletePriceRowEvent;
import by.weekmenu.adminka.ui.Events.IngredientChangedEvent;
import by.weekmenu.adminka.ui.beans.IngredientUOM;
import by.weekmenu.adminka.ui.views.CrudForm;
import com.vaadin.flow.component.AbstractField;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.HasValueAndElement;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.internal.AbstractFieldSupport;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.validator.RegexpValidator;
import com.vaadin.flow.shared.Registration;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

public class IngredientPriceEditor extends VerticalLayout implements HasValueAndElement<AbstractField.ComponentValueChangeEvent<IngredientPriceEditor, IngredientPriceDTO>, IngredientPriceDTO> {

    private final ComboBox<String> regionName;
    private final TextField priceValue;
    private final TextField currencyCode;
    private final ComboBox<String> unitOfMeasurefullName;
    private final TextField quantity;
    private final Button addRegion;
    private final Button deleteIngredientPrice;
    private final AbstractFieldSupport<IngredientPriceEditor, IngredientPriceDTO> fieldSupport;

    private final CrudForm<IngredientDTO, Long> form;
    private final Binder<IngredientPriceDTO> binder = new Binder<>();
    private IngredientPriceDTO ingredientPriceDTO;

    @Autowired
    public IngredientPriceEditor(CrudForm<IngredientDTO, Long> form,
                                 RegionService regionService,
                                 UnitOfMeasureService unitOfMeasureService,
                                 IngredientPricesEditor ingredientPricesEditor,
                                 IngredientPriceDTO ingredientPriceDTO) {
        this.fieldSupport =  new AbstractFieldSupport<>(this, null,
                Objects::equals, c ->  {});
        this.form = form;
        this.ingredientPriceDTO = ingredientPriceDTO;
        setSizeFull();
        setPadding(false);
        setMargin(false);

        regionName = new ComboBox<>("Регион");
        regionName.setId("RegionNameField");
        regionName.setItems(regionService.getAllDTOs()
                            .stream()
                            .map(RegionDTO::getName)
                            .collect(Collectors.toList()));
        priceValue = new TextField("Цена");
        priceValue.setId("PriceValueField");
        priceValue.setWidth("10%");
        priceValue.addValueChangeListener(e -> fireEvent(new IngredientChangedEvent(this)));
        binder.forField(priceValue)
                .withValidator(new RegexpValidator("Не более 7 цифр и не более 2-x десятичных знаков", "([0-9]{1,7})?([.,][0-9]{1,2})?"))
                .bind(IngredientPriceDTO::getPriceValueString, IngredientPriceDTO::setPriceValueString);

        currencyCode = new TextField("Валюта");
        currencyCode.setId("CurrencyCodeField");
        currencyCode.setWidth("10%");
        currencyCode.setReadOnly(true);
        currencyCode.addValueChangeListener(e -> fireEvent(new IngredientChangedEvent(this)));
        binder.bind(currencyCode, IngredientPriceDTO::getCurrencyCode, IngredientPriceDTO::setCurrencyCode);

        unitOfMeasurefullName = new ComboBox<>("Ед. измерения");
        unitOfMeasurefullName.setId("UnitOfMeasureField");
        unitOfMeasurefullName.setItems(unitOfMeasureService.getAllDTOs().stream()
                .map(UnitOfMeasureDTO::getFullName)
                .collect(Collectors.toList()));
        unitOfMeasurefullName.addValueChangeListener(e -> fireEvent(new IngredientChangedEvent(this)));
        binder.forField(unitOfMeasurefullName)
                .withValidator(this::checkEquivalent, "Для данной единицы измерения не введен эквивалент в граммах")
                .bind(IngredientPriceDTO::getUnitOfMeasureFullName, IngredientPriceDTO::setUnitOfMeasureFullName);

        quantity = new TextField("Кол-во");
        quantity.setId("QuantityField");
        quantity.setWidth("10%");
        binder.forField(quantity)
                .withValidator(new RegexpValidator("Не более 7 цифр и не более 1 десятичного знака", "([0-9]{1,7})?([.,][0-9]{1})?"))
                .bind(IngredientPriceDTO::getQuantityString, IngredientPriceDTO::setQuantityString);
        quantity.addValueChangeListener(e -> fireEvent(new IngredientChangedEvent(this)));

        addRegion = new Button(new Icon(VaadinIcon.PLUS));
        addRegion.setId("AddIngredientPriceButton");
        addRegion.setVisible(false);
        addRegion.addClickListener(e -> {
            ingredientPricesEditor.createEditor(null);
            addRegion.setVisible(false);
        });
        deleteIngredientPrice = new Button(new Icon(VaadinIcon.MINUS));
        deleteIngredientPrice.setId("DeleteIngredientPriceButton");
        deleteIngredientPrice.addClickListener(e->fireEvent(new DeletePriceRowEvent(this)));
        regionName.addValueChangeListener(e -> {
            String currencyCode1 = regionService.findRegionByName(e.getValue()).getCountryCurrencyCode();
            currencyCode.setValue(currencyCode1);
            fireEvent(new IngredientChangedEvent(this));
            addRegion.setVisible(true);
            deleteIngredientPrice.setVisible(true);
        });
        binder.forField(regionName)
                .withValidator(this::checkRegionName, "В одном регионе может быть только одна цена")
                .bind(IngredientPriceDTO::getRegionName, IngredientPriceDTO::setRegionName);
        HorizontalLayout row = new HorizontalLayout(regionName,
                priceValue, currencyCode, quantity, unitOfMeasurefullName, addRegion, deleteIngredientPrice);
        row.setWidthFull();
        add(row);
    }

    private boolean checkRegionName(String regionName) {
        if (form.getDTO().getIngredientPrices() != null) {
            List<String> regionNames = form.getDTO().getIngredientPrices().stream()
                    .map(IngredientPriceDTO::getRegionName)
                    .collect(Collectors.toList());
            Optional<String> regionNameFound = regionNames.stream().filter(regionName::equals).findAny();
            return !regionNameFound.isPresent();
        } else {
            return true;
        }
    }

    private boolean checkEquivalent(String unitOfMeasureFullName) {
        if (form.getDTO().getIngredientUOMS()!=null && unitOfMeasureFullName!=null) {
            Set<String> equivalentUoms = form.getDTO().getIngredientUOMS().stream()
                    .map(IngredientUOM::getUnitOfMeasureFullName)
                    .collect(Collectors.toSet());
            String result;
            result = equivalentUoms.stream().filter(unitOfMeasureFullName::equals).findAny().orElse(null);
            if (unitOfMeasureFullName.equals("Грамм")) {
                result = "Грамм";
            }
            return result != null;
        } else return form.getDTO().getIngredientUOMS() != null || unitOfMeasureFullName == null;
    }

    @Override
    public void setValue(IngredientPriceDTO ingredientPriceDTO) {
        if (ingredientPriceDTO!=null) {
            fieldSupport.setValue(ingredientPriceDTO);
            ingredientPriceDTO.setPriceValueString(String.valueOf(ingredientPriceDTO.getPriceValue()).replace('.',','));
            ingredientPriceDTO.setQuantityString(String.valueOf(ingredientPriceDTO.getQuantity()).replace('.',','));
            regionName.setValue(ingredientPriceDTO.getRegionName());
            priceValue.setValue(ingredientPriceDTO.getPriceValue().toString());
            unitOfMeasurefullName.setValue(ingredientPriceDTO.getUnitOfMeasureFullName());
            quantity.setValue(ingredientPriceDTO.getQuantity().toString());
            currencyCode.setValue(ingredientPriceDTO.getCurrencyCode());
        }
        binder.setBean(ingredientPriceDTO);
    }

    void setIngredientPrices() {
        if (form.getDTO()!=null) {
            Set<IngredientPriceDTO> ingredientPrices = form.getDTO().getIngredientPrices();
            if (ingredientPrices==null) {
                ingredientPrices = new HashSet<>();
            }
            if (ingredientPriceDTO==null) {
                ingredientPriceDTO = new IngredientPriceDTO();
            }
            ingredientPriceDTO.setRegionName(regionName.getValue());
            ingredientPriceDTO.setUnitOfMeasureFullName(unitOfMeasurefullName.getValue());
            ingredientPriceDTO.setCurrencyCode(currencyCode.getValue());
            ingredientPriceDTO.setPriceValue(new BigDecimal(priceValue.getValue().replace(',','.')));
            ingredientPriceDTO.setQuantity(new BigDecimal(quantity.getValue().replace(',','.')));
            ingredientPrices.add(ingredientPriceDTO);
            form.getDTO().setIngredientPrices(ingredientPrices);
        }
    }

    @Override
    public IngredientPriceDTO getValue() {
        return fieldSupport.getValue();
    }

    @Override
    public Registration addValueChangeListener(ValueChangeListener<? super AbstractField.ComponentValueChangeEvent<IngredientPriceEditor, IngredientPriceDTO>> valueChangeListener) {
        return fieldSupport.addValueChangeListener(valueChangeListener);
    }

    public Registration addIngredientChangedEvent(ComponentEventListener<IngredientChangedEvent> listener) {
        return addListener(IngredientChangedEvent.class, listener);
    }

    Registration addDeleteListener(ComponentEventListener<DeletePriceRowEvent> listener) {
        return addListener(DeletePriceRowEvent.class, listener);
    }

    public Binder<IngredientPriceDTO> getBinder() {
        return binder;
    }

    public IngredientPriceDTO getIngredientPriceDTO() {
        return ingredientPriceDTO;
    }

    Button getAddRegion() {
        return addRegion;
    }

    Button getDeleteIngredientPrice() {
        return deleteIngredientPrice;
    }
}
