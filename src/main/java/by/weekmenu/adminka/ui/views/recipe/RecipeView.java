package by.weekmenu.adminka.ui.views.recipe;

import by.weekmenu.adminka.DTO.RecipeDTO;
import by.weekmenu.adminka.DTO.RecipePriceDTO;
import by.weekmenu.adminka.service.RecipeService;
import by.weekmenu.adminka.ui.MainView;
import by.weekmenu.adminka.ui.util.AppConsts;
import by.weekmenu.adminka.ui.util.Converters;
import by.weekmenu.adminka.ui.views.CrudForm;
import by.weekmenu.adminka.ui.views.CrudView;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.util.Set;

@Route(value = AppConsts.PAGE_RECIPE, layout = MainView.class)
@PageTitle(AppConsts.TITLE_RECIPE)
public class RecipeView extends CrudView<RecipeDTO, Long> {

    private final Binder<RecipeDTO> binder;
    @Value("${no.image}")
    private String noImage;

    @Autowired
    public RecipeView(CrudForm<RecipeDTO, Long> form, RecipeService recipeServiceClient) {
        super(form, recipeServiceClient, null);
        binder = new Binder<>(RecipeDTO.class);
        getSearchBar().getSearchField().setVisible(false);
    }
    @Override
    public Binder<RecipeDTO> getBinder() {
        return binder;
    }

    @Override
    protected void setupGrid() {
        getGrid().addColumn(new ComponentRenderer<>(recipeDTO -> {
            if ((recipeDTO.getImageLink() != null) && !StringUtils.isEmpty(recipeDTO.getImageLink())) {
                Image image = new Image(recipeDTO.getImageLink(), recipeDTO.getName());
                image.setWidth("50px");
                image.setHeight("50px");
                image.addClassNames("small-image");
                return image;
            } else {
                Image imageEmpty = new Image(noImage, "No image");
                imageEmpty.setHeight("50px");
                imageEmpty.setWidth("50px");
                return imageEmpty;
            }
        }
        ))
                .setHeader("Фото");
        getGrid().addColumn(RecipeDTO::getName).setHeader("Рецепт");
        getGrid().addColumn(recipeDTO ->
                String.join(" / ",
                        Converters.convertBigDecimalToString(recipeDTO.getCalories()),
                        Converters.convertBigDecimalToString(recipeDTO.getProteins()),
                        Converters.convertBigDecimalToString(recipeDTO.getFats()),
                        Converters.convertBigDecimalToString(recipeDTO.getCarbs())
                )).setHeader("К/Б/Ж/У в 1 порции");
        getGrid().addColumn(recipeDTO -> Converters.convertBigDecimalToString(recipeDTO.getGramsPerPortion()))
                .setHeader("Гр. в порции");
        getGrid().addColumn(recipeDTO -> Integer.valueOf(recipeDTO.getCookingTime())
                + Integer.valueOf(recipeDTO.getPreparingTime()))
                .setHeader("Время приготовления, мин.").setSortable(true);
        getGrid().addColumn(new ComponentRenderer<>(this::showRegionPrices)).setHeader("Стоимость 1 порции");
    }

    private Component showRegionPrices(RecipeDTO recipeDTO) {
        VerticalLayout layout = new VerticalLayout();
        Set<RecipePriceDTO> recipePrices = recipeDTO.getRecipePrices();
        for (RecipePriceDTO recipePriceDto : recipePrices) {
            layout.add(new HorizontalLayout(new Label(recipePriceDto.getRegionName()),
                    new Label(recipePriceDto.getPriceValue().replace('.',',')), new Label(recipePriceDto.getCurrencyCode())));
        }
        return layout;
    }
}
