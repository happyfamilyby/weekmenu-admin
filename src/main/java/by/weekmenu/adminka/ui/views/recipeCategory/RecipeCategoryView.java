package by.weekmenu.adminka.ui.views.recipeCategory;

import by.weekmenu.adminka.DTO.RecipeCategoryDTO;
import by.weekmenu.adminka.service.RecipeCategoryService;
import by.weekmenu.adminka.ui.MainView;
import by.weekmenu.adminka.ui.util.AppConsts;
import by.weekmenu.adminka.ui.views.CrudForm;
import by.weekmenu.adminka.ui.views.CrudView;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;

@Route(value = AppConsts.PAGE_RECIPECATEGORY, layout = MainView.class)
@PageTitle(AppConsts.TITLE_RECIPECATEGORY)
public class RecipeCategoryView extends CrudView<RecipeCategoryDTO, Long> {

    private final Binder<RecipeCategoryDTO> binder;

    @Autowired
    public RecipeCategoryView(CrudForm<RecipeCategoryDTO, Long> form, RecipeCategoryService serviceClient) {
        super(form, serviceClient, null);
        binder = new Binder<>(RecipeCategoryDTO.class);
        if(serviceClient.getAllDTOs()!=null) {
            getGrid().setItems(serviceClient.getAllDTOs());
        }
        getSearchBar().getSearchField().setVisible(false);
    }

    @Override
    public Binder<RecipeCategoryDTO> getBinder() {
        return binder;
    }

    @Override
    protected void setupGrid() {
        getGrid().addColumn(RecipeCategoryDTO::getName).setHeader("Категория рецепта");
    }
}
