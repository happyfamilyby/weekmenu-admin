package by.weekmenu.adminka.ui.views.recycleBin;

import by.weekmenu.adminka.DTO.RecycleBinDTO;
import by.weekmenu.adminka.service.RecycleBinService;
import by.weekmenu.adminka.ui.MainView;
import by.weekmenu.adminka.ui.util.AppConsts;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.renderer.LocalDateTimeRenderer;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.claspina.confirmdialog.ButtonOption;
import org.claspina.confirmdialog.ConfirmDialog;

import java.time.format.DateTimeFormatter;

@Route(value = AppConsts.PAGE_RECYCLE_BIN, layout = MainView.class)
@PageTitle(AppConsts.TITLE_RECYCLE_BIN)
public class RecycleBinView  extends VerticalLayout {

    private final Grid<RecycleBinDTO> grid;
    private final RecycleBinService recycleBinService;

    public RecycleBinView(RecycleBinService recycleBinService) {
        this.recycleBinService = recycleBinService;
        setSizeFull();
        setDefaultHorizontalComponentAlignment(Alignment.CENTER);
        grid = new Grid<>();
        grid.setItems(recycleBinService.getAllBins());
        setupGrid();
        add(grid);
    }

    private void setupGrid() {
        grid.addColumn(RecycleBinDTO::getElementName).setHeader("Элемент");
        grid.addColumn(RecycleBinDTO::getEntityName).setHeader("Таблица");
        grid.addColumn(new LocalDateTimeRenderer<>(RecycleBinDTO::getDeleteDate,
                DateTimeFormatter.ofPattern("dd.MM.yyy HH:mm")))
                .setHeader("Дата удаления");
        grid.addComponentColumn(item ->
                new HorizontalLayout(createDeleteButton(grid, item), createRestoreButton(grid, item)))
                .setHeader("Действия").setFlexGrow(2);
    }

    private Button createDeleteButton(Grid<RecycleBinDTO> grid, RecycleBinDTO recycleBinDTO) {
        Button deleteButton = new Button("Удалить", click -> ConfirmDialog.createWarning()
                .withCaption("Удалить навсегда?")
                .withMessage("Внимание: данное действие нельзя отменить.")
                .withOkButton(() -> {
                    recycleBinService.delete(recycleBinDTO.getId());
                    grid.setItems(recycleBinService.getAllBins());
                    Notification.show(recycleBinDTO.getElementName() + " удалён");
                }, ButtonOption.caption("Удалить"))
                .withCancelButton(ButtonOption.caption("Отменить"))
                .open());
        deleteButton.setId("RecycleBinDeleteButton");
        return deleteButton;
    }

    private Button createRestoreButton(Grid<RecycleBinDTO> grid, RecycleBinDTO recycleBinDTO) {
        Button restoreButton = new Button("Восстановить", click -> {
            recycleBinService.restore(recycleBinDTO.getId());
            Notification.show(recycleBinDTO.getElementName() + " восстановлен");
            grid.setItems(recycleBinService.getAllBins());
        });
        restoreButton.setId("RecycleBinRestoreButton");
        return restoreButton;
    }
}
