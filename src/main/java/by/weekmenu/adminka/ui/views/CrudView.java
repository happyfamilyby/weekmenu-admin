package by.weekmenu.adminka.ui.views;

import by.weekmenu.adminka.service.CrudServiceClient;
import by.weekmenu.adminka.ui.components.SearchBar;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.html.NativeButton;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.binder.Binder;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

@HtmlImport("frontend://custom-material-dialog.html")
public abstract class CrudView<E, ID> extends VerticalLayout {

    private final CrudServiceClient<E, ID> crudService;
    private final Dialog dialog = new Dialog();
    private final CrudForm<E, ID> form;
    private final Grid<E> grid;
    private final Component filterComponent;
    private final SearchBar searchBar;
    private final H1 h1;

    protected CrudView(CrudForm<E, ID> form, CrudServiceClient<E, ID> crudService, Component filterComponent) {
        this.form = form;
        this.crudService = crudService;
        this.filterComponent = filterComponent;
        h1 = new H1("Пока ничего не добавлено");

        setSizeFull();
        setDefaultHorizontalComponentAlignment(Alignment.CENTER);

        dialog.add((Component) getForm());
        searchBar = new SearchBar(this, filterComponent);

        grid = new Grid<>();
        setupGrid();
        updateList(null);

        grid.asSingleSelect().addValueChangeListener(e -> {
            getForm().setBinder(getBinder(), e.getValue());
            getBinder().readBean(e.getValue());
            getForm().getButtons().getDeleteButton().setVisible(true);
            dialog.open();
        });

        //drag and drop columns order
        grid.setColumnReorderingAllowed(true);

        setupEventListeners();
        grid.getDataProvider().refreshAll();

        getForm().getButtons().getSaveButton().addClickShortcut(Key.ENTER);
    }

    private void setupEventListeners() {
        getForm().getButtons().addSaveListener(e -> save());
        getForm().getButtons().addCancelListener(e -> cancel());
        getForm().getButtons().addDeleteListener(e -> delete());

        getDialog().getElement().addEventListener("opened-changed", e -> {
            if (!getDialog().isOpened()) {
                cancel();
            }
        });
    }

    protected void save() {
        E entityDTO = getForm().getDTO();
        boolean isValid = getBinder().writeBeanIfValid(entityDTO);
        if (isValid) {
            if (getForm().getDTOId() == null) {
                try {
                    E savedEntity = crudService.createDTO(entityDTO);
                    if (savedEntity != null) {
                        Notification.show(getForm().getChangedDTOName() + " сохранён");
                        closeUpdate();
                    }
                } catch (Exception e) {
                    errorNotification(e);
                }
            } else {
                try {
                    crudService.updateDTO(entityDTO);
                    Notification.show(getForm().getChangedDTOName() + " обновлён");
                    closeUpdate();
                } catch (Exception e) {
                    errorNotification(e);
                }
            }
        }
    }

    public static void errorNotification(Exception e) {
        e.printStackTrace();
        NativeButton button = new NativeButton("Закрыть");
        Label error = new Label(e.toString());
        Notification notification = new Notification(error, button);
        notification.setPosition(Notification.Position.TOP_STRETCH);
        notification.open();
        button.addClickListener(event -> notification.close());
    }

    private void cancel() {
        getGrid().asSingleSelect().clear();
        getDialog().close();
        updateList(null);
        getGrid().getDataProvider().refreshAll();
    }

    private void delete() {
        E entityDTO = getForm().getDTO();
        try {
            crudService.deleteDTO(entityDTO);
            closeUpdate();
        } catch (Exception e) {
            errorNotification(e);
        }
    }

    protected void closeUpdate() {
        getGrid().asSingleSelect().clear();
        getDialog().close();
        updateList(null);
        grid.getDataProvider().refreshAll();
    }

    public void updateList(String search) {
        if (getFilterComponent() == null) {
            if (StringUtils.isEmpty(search)) {
                List<E> list = crudService.getAllDTOs();
                if (list.size() > 0) {
                    grid.setItems(list);
                    remove(h1);
                    add(searchBar, grid);
                } else {
                    remove(grid);
                    add(searchBar, h1);
                }
            }
        }
    }

    public abstract Binder<E> getBinder();

    protected abstract void setupGrid();

    public CrudForm<E, ID> getForm() {
        return form;
    }

    public Dialog getDialog() {
        return dialog;
    }

    public Grid<E> getGrid() {
        return grid;
    }

    public CrudServiceClient<E, ID> getCrudService() {
        return crudService;
    }

    protected SearchBar getSearchBar() {
        return searchBar;
    }

    private Component getFilterComponent() {
        return filterComponent;
    }
}
