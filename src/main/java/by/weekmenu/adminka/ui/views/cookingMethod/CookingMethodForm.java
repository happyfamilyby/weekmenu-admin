package by.weekmenu.adminka.ui.views.cookingMethod;

import by.weekmenu.adminka.DTO.CookingMethodDTO;
import by.weekmenu.adminka.service.CookingMethodService;
import by.weekmenu.adminka.ui.components.FormButtonsBar;
import by.weekmenu.adminka.ui.views.CrudForm;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.spring.annotation.SpringComponent;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CookingMethodForm extends VerticalLayout implements CrudForm<CookingMethodDTO, Integer> {

    private final TextField name;
    private final FormButtonsBar buttons;
    private CookingMethodDTO cookingMethodDTO;
    private final CookingMethodService cookingMethodService;

    public CookingMethodForm(CookingMethodService cookingMethodService) {
        this.cookingMethodService = cookingMethodService;
        setSizeFull();
        setId("CookingMethodForm");
        name = new TextField("Способ приготовления рецепта");
        name.focus();
        name.setWidth("100%");
        name.setId("CookingMethodNameField");
        buttons = new FormButtonsBar();
        add(name, buttons);
    }

    @Override
    public FormButtonsBar getButtons() {
        return buttons;
    }

    @Override
    public void setBinder(Binder<CookingMethodDTO> binder, CookingMethodDTO entity) {
        this.cookingMethodDTO = entity;
        binder.forField(name)
                .asRequired("Поле не может быть пустым")
                .withValidator(field -> field.trim().length() > 0, "Поле не может быть пустым")
                .withValidator(field -> field.length() <= 255, "Не более 255 символов")
                .withValidator(field -> {
                    if (name.getValue().equals(cookingMethodDTO.getName())) {
                        return true;
                    } else {
                        return cookingMethodService.checkCookingMethodUniqueName(field) == 0;
                    }
                }, "Данное название способа приготовления рецепта уже внесено в базу")
                .bind(CookingMethodDTO::getName, CookingMethodDTO::setName);
    }

    @Override
    public CookingMethodDTO getDTO() {
        return cookingMethodDTO;
    }

    @Override
    public String getChangedDTOName() {
        return cookingMethodDTO.getName();
    }

    @Override
    public Integer getDTOId() {
        return cookingMethodDTO.getId();
    }
}
