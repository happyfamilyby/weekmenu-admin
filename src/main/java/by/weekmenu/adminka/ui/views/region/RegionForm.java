package by.weekmenu.adminka.ui.views.region;

import by.weekmenu.adminka.DTO.RegionDTO;
import by.weekmenu.adminka.service.CountryService;
import by.weekmenu.adminka.service.RegionService;
import by.weekmenu.adminka.ui.components.FormButtonsBar;
import by.weekmenu.adminka.ui.views.CrudForm;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.spring.annotation.SpringComponent;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;


@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class RegionForm extends VerticalLayout implements CrudForm<RegionDTO, Long> {

    private final TextField name;
    private final ComboBox<String> country;

    private final FormButtonsBar buttons;
    private RegionDTO regionDTO;
    private final RegionService regionService;

    public RegionForm(RegionService regionService, CountryService countryService) {
        this.regionService = regionService;
        setSizeFull();
        setId("RegionForm");
        name = new TextField("Регион");
        name.focus();
        name.setWidth("100%");
        name.setId("RegionNameField");

        country = new ComboBox<>("Страна");
        country.setItems(countryService.getAllCountryNames());
        country.setWidth("100%");
        country.setId("RegionCountryField");

        buttons = new FormButtonsBar();
        add(name, country, buttons);
    }

    @Override
    public FormButtonsBar getButtons() {
        return buttons;
    }

    @Override
    public void setBinder(Binder<RegionDTO> binder, RegionDTO entity) {
        this.regionDTO = entity;
        binder.forField(name)
                .asRequired("Поле не может быть пустым")
                .withValidator(field -> field.trim().length() > 0, "Поле не может быть пустым")
                .withValidator(field -> field.length() <= 255, "Не более 255 символов")
                .withValidator(field -> {
                    if (name.getValue().equals(regionDTO.getName())) {
                        return true;
                    } else {
                        return regionService.checkUniqueName(field) == 0;
                    }
                }, "Данное название региона уже внесено в базу")
                .bind(RegionDTO::getName, RegionDTO::setName);
        binder.forField(country)
                .asRequired("Поле не может быть пустым")
                .bind(RegionDTO::getCountryName, RegionDTO::setCountryName);
    }

    @Override
    public RegionDTO getDTO() {
        return regionDTO;
    }

    @Override
    public String getChangedDTOName() {
        return regionDTO.getName();
    }

    @Override
    public Long getDTOId() {
        return regionDTO.getId();
    }
}
