package by.weekmenu.adminka.ui.views.ingredient;

import by.weekmenu.adminka.DTO.IngredientDTO;
import by.weekmenu.adminka.DTO.IngredientPriceDTO;
import by.weekmenu.adminka.service.RegionService;
import by.weekmenu.adminka.service.UnitOfMeasureService;
import by.weekmenu.adminka.ui.views.CrudForm;
import com.vaadin.flow.component.AbstractField;
import com.vaadin.flow.component.HasValueAndElement;
import com.vaadin.flow.component.internal.AbstractFieldSupport;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.shared.Registration;

import java.util.*;
import java.util.stream.Collectors;

class IngredientPricesEditor extends VerticalLayout implements HasValueAndElement<AbstractField.ComponentValueChangeEvent<IngredientPricesEditor, Set<IngredientPriceDTO>>, Set<IngredientPriceDTO>> {

    private final CrudForm<IngredientDTO, Long> form;

    private final RegionService regionService;
    private final UnitOfMeasureService unitOfMeasureService;
    private int numberOfFieldsChanged;

    private final AbstractFieldSupport<IngredientPricesEditor, Set<IngredientPriceDTO>> fieldSupport;

    public IngredientPricesEditor(RegionService regionService,
                                  UnitOfMeasureService unitOfMeasureService,
                                  CrudForm<IngredientDTO, Long> form) {
        this.form = form;
        setSizeFull();
        setSpacing(false);
        setPadding(false);
        this.regionService = regionService;
        this.unitOfMeasureService = unitOfMeasureService;
        this.fieldSupport = new AbstractFieldSupport<>(this, Collections.emptySet(),
                Objects::equals, c ->  {});
    }

    @Override
    public void setValue(Set<IngredientPriceDTO> ingredientPriceDTOS) {
        fieldSupport.setValue(ingredientPriceDTOS);
        removeAll();
        if (ingredientPriceDTOS!=null) {
            List<IngredientPriceDTO> list = new ArrayList<>(ingredientPriceDTOS);
            for (int i=0; i<list.size(); i++) {
                IngredientPriceEditor editor = createEditor(list.get(i));
                editor.getAddRegion().setVisible(false);
                if (i==list.size()-1) {
                    editor.getAddRegion().setVisible(true);
                }
            }
            if (ingredientPriceDTOS.size()==0) {
                createEditor(null);
            }
        }
        numberOfFieldsChanged = 0;
    }

    IngredientPriceEditor createEditor(IngredientPriceDTO ingredientPriceDTO) {
        IngredientPriceEditor editor = new IngredientPriceEditor(form, regionService, unitOfMeasureService, this, ingredientPriceDTO);
        getElement().appendChild(editor.getElement());
        editor.addIngredientChangedEvent(e -> ingredientChanged(e.getSource()));
        editor.addDeleteListener(e-> {
           remove(e.getSource());
           deleteIngredientPrice(e.getSource().getIngredientPriceDTO());
        });
        editor.setValue(ingredientPriceDTO);
        if (ingredientPriceDTO==null) {
            editor.getDeleteIngredientPrice().setVisible(false);
        }
        return editor;
    }

    private void ingredientChanged(IngredientPriceEditor ingredientPriceEditor) {
        if(ingredientPriceEditor.getIngredientPriceDTO()==null) {
            numberOfFieldsChanged++;
            if (ingredientPriceEditor.getBinder().validate().isOk()) {
                if (numberOfFieldsChanged == 5) {
                    ingredientPriceEditor.setIngredientPrices();
                    numberOfFieldsChanged = 0;
                }
            }
        }
    }

    @Override
    public Set<IngredientPriceDTO> getValue() {
        return fieldSupport.getValue();
    }

    @Override
    public Registration addValueChangeListener(ValueChangeListener<? super AbstractField.ComponentValueChangeEvent<IngredientPricesEditor, Set<IngredientPriceDTO>>> valueChangeListener) {
        return fieldSupport.addValueChangeListener(valueChangeListener);
    }

    private void deleteIngredientPrice(IngredientPriceDTO ingredientPriceDTO) {
        if (form.getDTO().getIngredientPrices()!=null) {
            setValue(form.getDTO().getIngredientPrices().stream()
                    .filter(element -> element != ingredientPriceDTO).collect(Collectors.toSet()));
            form.getDTO().setIngredientPrices(getValue());
        }
    }
}
