package by.weekmenu.adminka.ui.views.currency;

import by.weekmenu.adminka.DTO.CurrencyDTO;
import by.weekmenu.adminka.service.CurrencyService;
import by.weekmenu.adminka.ui.MainView;
import by.weekmenu.adminka.ui.util.AppConsts;
import by.weekmenu.adminka.ui.views.CrudForm;
import by.weekmenu.adminka.ui.views.CrudView;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;

@Route(value = AppConsts.PAGE_CURRENCY, layout = MainView.class)
@PageTitle(AppConsts.TITLE_CURRENCY)
public class CurrencyView extends CrudView<CurrencyDTO, Integer> {

    private final Binder<CurrencyDTO> binder;

    @Autowired
    public CurrencyView(CrudForm<CurrencyDTO, Integer> form, CurrencyService currencyService) {
        super(form, currencyService, null);
        binder = new Binder<>(CurrencyDTO.class);
        if (currencyService.getAllDTOs()!=null) {
            getGrid().setItems(currencyService.getAllDTOs());
        }
        getSearchBar().getSearchField().setVisible(false);
    }

    @Override
    public Binder<CurrencyDTO> getBinder() {
        return binder;
    }

    @Override
    protected void setupGrid() {
        getGrid().addColumn(CurrencyDTO::getName).setHeader("Валюта");
        getGrid().addColumn(CurrencyDTO::getCode).setHeader("Код");
    }
}
