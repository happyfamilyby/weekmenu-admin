package by.weekmenu.adminka.ui.views.menu;

import by.weekmenu.adminka.DTO.RecipeDTO;
import by.weekmenu.adminka.service.RecipeService;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.templatemodel.TemplateModel;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;

import java.util.stream.Collectors;

/**
 * A Designer generated component for the menu-edit-view template.
 *
 * Designer will add and remove fields with @Id mappings but
 * does not overwrite or otherwise change this file.
 */
@Tag("menu-edit-view")
@JsModule("./src/menu-edit-view.js")
@Route("menu/edit")
public class MenuEditView extends PolymerTemplate<MenuEditView.MenuEditViewModel> {

    @Id("vaadinGrid")
    private Grid<RecipeDTO> vaadinGrid;

    /**
     * Creates a new MenuEditView.
     */
    public MenuEditView(RecipeService recipeService) {
        vaadinGrid.setItems(recipeService.getAllDTOs());
        vaadinGrid.addColumn(RecipeDTO::getName).setHeader("Рецепт");
    }

    /**
     * This model binds properties between MenuEditView and menu-edit-view
     */
    public interface MenuEditViewModel extends TemplateModel {
        // Add setters and getters for template properties here.
    }
}
