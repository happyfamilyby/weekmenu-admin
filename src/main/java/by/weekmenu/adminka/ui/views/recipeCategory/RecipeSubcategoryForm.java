package by.weekmenu.adminka.ui.views.recipeCategory;

import by.weekmenu.adminka.DTO.RecipeSubcategoryDTO;
import by.weekmenu.adminka.service.RecipeSubcategoryService;
import by.weekmenu.adminka.ui.components.FormButtonsBar;
import by.weekmenu.adminka.ui.views.CrudForm;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.spring.annotation.SpringComponent;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class RecipeSubcategoryForm extends VerticalLayout implements CrudForm<RecipeSubcategoryDTO, Long> {

    private final TextField name;
    private final FormButtonsBar buttons;
    private RecipeSubcategoryDTO recipeSubcategoryDTO;
    private final RecipeSubcategoryService recipeSubcategoryService;

    public RecipeSubcategoryForm(RecipeSubcategoryService recipeSubcategoryService) {
        this.recipeSubcategoryService = recipeSubcategoryService;
        setSizeFull();
        setId("RecipeSubcategoryForm");
        name = new TextField("Подкатегория рецепта");
        name.focus();
        name.setWidth("100%");
        name.setId("RecipeSubcategoryNameField");

        buttons = new FormButtonsBar();
        add(name, buttons);
    }

    @Override
    public FormButtonsBar getButtons() {
        return buttons;
    }

    @Override
    public void setBinder(Binder<RecipeSubcategoryDTO> binder, RecipeSubcategoryDTO entity) {
        this.recipeSubcategoryDTO = entity;
        binder.forField(name)
                .asRequired("Поле не может быть пустым")
                .withValidator(field -> field.trim().length() > 0, "Поле не может быть пустым")
                .withValidator(field -> field.length() <= 255, "Не более 255 символов")
                .withValidator(field -> {
                    if (name.getValue().equals(recipeSubcategoryDTO.getName())) {
                        return true;
                    } else {
                        return recipeSubcategoryService.checkRecipeSubcategoryUniqueName(field) == 0;
                    }
                }, "Данное название подкатегории рецепта уже внесено в базу")
                .bind(RecipeSubcategoryDTO::getName, RecipeSubcategoryDTO::setName);
    }

    @Override
    public RecipeSubcategoryDTO getDTO() {
        return recipeSubcategoryDTO;
    }

    @Override
    public String getChangedDTOName() {
        return recipeSubcategoryDTO.getName();
    }

    @Override
    public Long getDTOId() {
        return recipeSubcategoryDTO.getId();
    }
}
 