package by.weekmenu.adminka.ui.views.recipe;

import by.weekmenu.adminka.DTO.*;
import by.weekmenu.adminka.service.*;
import by.weekmenu.adminka.ui.components.FormButtonsBar;
import by.weekmenu.adminka.ui.components.ImageUpload;
import by.weekmenu.adminka.ui.views.CrudForm;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.html.H4;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.validator.RegexpValidator;
import com.vaadin.flow.data.validator.StringLengthValidator;
import com.vaadin.flow.spring.annotation.SpringComponent;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.vaadin.gatanaso.MultiselectComboBox;

import java.util.Set;
import java.util.stream.Collectors;

@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class RecipeForm extends VerticalLayout implements CrudForm<RecipeDTO, Long> {

    private final TextField name;
    private final NumberField portions;
    private final TextField cookingTime;
    private final TextField preparingTime;
    private final TextField imageLink;
    private final TextField source;
    private final ComboBox<String> cookingMethodName;
    private final MultiselectComboBox<String> categories;
    private final MultiselectComboBox<String> subcategories;
    private final RecipeIngredientsEditor recipeIngredientsEditor;
    private final CookingStepsEditor cookingStepsEditor;
    private final RecipeService recipeService;

    private RecipeDTO recipeDto;
    private final FormButtonsBar buttons;

    public RecipeForm(@Value("${upload.location}") String uploadLocation,
                      @Value("${images.path.pattern}") String pathPattern,
                      IngredientService ingredientService,
                      RecipeService recipeService,
                      RecipeCategoryService recipeCategoryService,
                      RecipeSubcategoryService recipeSubcategoryService,
                      CookingMethodService cookingMethodService) {
        this.recipeService = recipeService;
        setSizeFull();
        setId("RecipeForm");
        name = new TextField("Рецепт");
        name.focus();
        name.setWidth("50%");
        name.setId("RecipeNameField");
        setAlignSelf(Alignment.CENTER, name);
        portions = new NumberField("Кол-во порций");
        portions.setId("RecipePortionsField");
        portions.setValue(1d);
        portions.setMin(0);
        portions.setHasControls(true);
        cookingTime = new TextField("Время приготовления, мин.");
        cookingTime.setId("RecipeCookingTimeField");
        cookingTime.setWidth("40%");
        preparingTime = new TextField("Время подготовки, мин.");
        preparingTime.setId("RecipePreparingTimeField");
        preparingTime.setWidth("40%");
        imageLink = new TextField("Фото");
        imageLink.setWidth("70%");
        imageLink.setId("RecipeImageLinkField");
        ImageUpload imageUpload = new ImageUpload(uploadLocation, pathPattern);
        imageUpload.uploadImage("Recipes", imageLink);
        imageUpload.setAcceptedFileTypes("image/jpeg", "image/png", "image/gif");
        HorizontalLayout chosePic = new HorizontalLayout(imageLink, imageUpload);
        chosePic.setSizeFull();
        cookingMethodName = new ComboBox<>("Способ приготовления");
        cookingMethodName.setId("RecipeCookingMethodNameField");
        source = new TextField("Источник рецепта");
        source.setId("RecipeSourceField");
        source.setWidth("70%");
        HorizontalLayout row1 = new HorizontalLayout(portions, cookingTime, preparingTime);
        row1.setSizeFull();
        row1.setJustifyContentMode(JustifyContentMode.START);
        HorizontalLayout row2 = new HorizontalLayout(source, cookingMethodName);
        row2.setSizeFull();
        categories = new MultiselectComboBox<>();
        categories.setLabel("Категории");
        categories.setItems(recipeCategoryService.getAllDTOs()
                .stream()
                .map(RecipeCategoryDTO::getName)
                .collect(Collectors.toSet()));
        subcategories = new MultiselectComboBox<>();
        subcategories.setLabel("Подкатегории");
        subcategories.setItems(recipeSubcategoryService.getAllDTOs()
                .stream()
                .map(RecipeSubcategoryDTO::getName)
                .collect(Collectors.toSet()));
        HorizontalLayout row3 = new HorizontalLayout(categories, subcategories);
        cookingMethodName.setItems(cookingMethodService.getAllDTOs()
                .stream()
                .map(CookingMethodDTO::getName)
                .collect(Collectors.toSet()));
        recipeIngredientsEditor = new RecipeIngredientsEditor(ingredientService, this);
        cookingStepsEditor = new CookingStepsEditor(uploadLocation, pathPattern,this);
        this.buttons = new FormButtonsBar();
        add(name, chosePic, row1, row2, row3, new H4("Ингредиенты"),
                recipeIngredientsEditor, new H4("Шаги приготовления"), cookingStepsEditor, buttons);
    }

    @Override
    public FormButtonsBar getButtons() {
        return buttons;
    }

    @Override
    public void setBinder(Binder<RecipeDTO> binder, RecipeDTO entity) {
        this.recipeDto = entity;
        if (recipeDto != null && recipeDto.getId() != null) {
            recipeDto.setRecipeIngredients(entity.getRecipeIngredients());
            recipeDto.setCookingSteps(entity.getCookingSteps());
        } else {
            recipeIngredientsEditor.createEditor(null);
            cookingStepsEditor.createEditor(null);
        }
        binder.forField(name)
                .asRequired("Поле не может быть пустым")
                .withValidator(field -> field.trim().length() > 0, "Поле не может быть пустым")
                .withValidator(new StringLengthValidator("Не более 255 символов", 1, 255))
                .withValidator(field -> {
                    if (name.getValue().equals(recipeDto.getName())) {
                        return true;
                    } else {
                        return recipeService.checkUniqueName(field) == 0;
                    }
                }, "Данное название рецепта уже внесено в базу")
                .bind(RecipeDTO::getName, RecipeDTO::setName);
        binder.forField(portions)
                .bind(RecipeDTO::getPortions, RecipeDTO::setPortions);
        binder.forField(cookingTime)
                .withValidator(new RegexpValidator("Должно быть положительное число", "^[0-9]*$"))
                .withValidator(field -> {
                    if (!StringUtils.isEmpty(field)) {
                        return Integer.valueOf(field) <= 32000;
                    } else {
                        return true;
                    }
                }, "Не более 32 тыс.")
                .bind(RecipeDTO::getCookingTime, RecipeDTO::setCookingTime);
        binder.forField(preparingTime)
                .withValidator(new RegexpValidator("Должно быть положительное число", "^[0-9]*$"))
                .withValidator(field -> {
                    if (!StringUtils.isEmpty(field)) {
                        return Integer.valueOf(field) <= 32000;
                    } else {
                        return true;
                    }
                }, "Не более 32 тыс.")
                .bind(RecipeDTO::getPreparingTime, RecipeDTO::setPreparingTime);
        binder.forField(imageLink)
                .withValidator(new StringLengthValidator("Не более 255 символов", 0, 255))
                .bind(RecipeDTO::getImageLink, RecipeDTO::setImageLink);
        binder.forField(cookingMethodName).bind(RecipeDTO::getCookingMethodName, RecipeDTO::setCookingMethodName);
        binder.forField(source)
                .withValidator(new StringLengthValidator("Не более 255 символов", 0, 255))
                .bind(RecipeDTO::getSource, RecipeDTO::setSource);
        binder.forField(recipeIngredientsEditor).bind(RecipeDTO::getRecipeIngredients, null);
        binder.forField(cookingStepsEditor)
                .bind(RecipeDTO::getCookingSteps, null);
        binder.forField(categories).bind(RecipeDTO::getCategoryNames, RecipeDTO::setCategoryNames);
        binder.forField(subcategories).bind(RecipeDTO::getSubcategoryNames, RecipeDTO::setSubcategoryNames);
    }

    @Override
    public RecipeDTO getDTO() {
        if (recipeDto.getRecipeIngredients()!=null) {
            Set<RecipeIngredientDTO> recipeIngredients = recipeDto.getRecipeIngredients();
            for (RecipeIngredientDTO temp : recipeIngredients) {
                if (temp.getQuantity()!=null) {
                    temp.setQuantity(temp.getQuantity().replace(',','.'));
                }
            }
        }
        return recipeDto;
    }

    @Override
    public String getChangedDTOName() {
        return recipeDto.getName();
    }

    @Override
    public Long getDTOId() {
        return recipeDto.getId();
    }

    public String getNameFieldValue() {
        return name.getValue();
    }
}
