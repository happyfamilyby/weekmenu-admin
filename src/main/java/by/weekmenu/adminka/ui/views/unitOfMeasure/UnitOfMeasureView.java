package by.weekmenu.adminka.ui.views.unitOfMeasure;

import by.weekmenu.adminka.DTO.UnitOfMeasureDTO;
import by.weekmenu.adminka.service.UnitOfMeasureService;
import by.weekmenu.adminka.ui.MainView;
import by.weekmenu.adminka.ui.util.AppConsts;
import by.weekmenu.adminka.ui.views.CrudForm;
import by.weekmenu.adminka.ui.views.CrudView;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;

@Route(value = AppConsts.PAGE_UOM, layout = MainView.class)
@PageTitle(AppConsts.TITLE_UOM)
public class UnitOfMeasureView extends CrudView<UnitOfMeasureDTO, Long> {

    private final Binder<UnitOfMeasureDTO> binder;

    @Autowired
    public UnitOfMeasureView(CrudForm<UnitOfMeasureDTO, Long> form, UnitOfMeasureService serviceClient) {
        super(form, serviceClient, null);
        binder = new Binder<>(UnitOfMeasureDTO.class);
        if(serviceClient.getAllDTOs()!=null) {
            getGrid().setItems(serviceClient.getAllDTOs());
        }
        getSearchBar().getSearchField().setVisible(false);
    }

    @Override
    public Binder<UnitOfMeasureDTO> getBinder() {
        return binder;
    }

    @Override
    protected void setupGrid() {
        getGrid().addColumn(UnitOfMeasureDTO::getFullName).setHeader("Полное название");
        getGrid().addColumn(UnitOfMeasureDTO::getShortName).setHeader("Сокращённое название");
    }
}
