package by.weekmenu.adminka.ui.views.recipe;

import by.weekmenu.adminka.DTO.IngredientDTO;
import by.weekmenu.adminka.DTO.RecipeDTO;
import by.weekmenu.adminka.DTO.RecipeIngredientDTO;
import by.weekmenu.adminka.service.IngredientService;
import by.weekmenu.adminka.ui.Events.RecipeChangedEvent;
import by.weekmenu.adminka.ui.views.CrudForm;
import com.vaadin.flow.component.AbstractField;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.HasValueAndElement;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.internal.AbstractFieldSupport;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.validator.RegexpValidator;
import com.vaadin.flow.shared.Registration;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class RecipeIngredientEditor extends VerticalLayout implements HasValueAndElement<AbstractField.ComponentValueChangeEvent<RecipeIngredientEditor, RecipeIngredientDTO>, RecipeIngredientDTO> {

    private final ComboBox<String> ingredientName;
    private final TextField quantity;
    private final ComboBox<String> unitOfMeasureName;
    private final Button addIngredient;
    private final Button deleteIngredient;
    private final AbstractFieldSupport<RecipeIngredientEditor, RecipeIngredientDTO> fieldSupport;
    private final Binder<RecipeIngredientDTO> binder = new Binder<>();
    private RecipeIngredientDTO recipeIngredientDTO;
    private final CrudForm<RecipeDTO, Long> form;

    public RecipeIngredientEditor(CrudForm<RecipeDTO, Long> form,
                                  IngredientService ingredientService,
                                  RecipeIngredientDTO recipeIngredientDTO,
                                  RecipeIngredientsEditor editor) {
        setSizeFull();
        setPadding(false);
        setMargin(false);
        this.fieldSupport =  new AbstractFieldSupport<>(this, null,
                Objects::equals, c ->  {});
        this.recipeIngredientDTO = recipeIngredientDTO;
        this.form = form;

        ingredientName = new ComboBox<>("Ингредиент");
        ingredientName.setId("IngredientNameField");
        ingredientName.setItems(ingredientService.getAllDTOs()
                .stream()
                .map(IngredientDTO::getName)
                .collect(Collectors.toList()));
        quantity = new TextField("Кол-во");
        quantity.setId("QuantityField");
        unitOfMeasureName = new ComboBox<>("Ед. измерения");
        unitOfMeasureName.setId("UnitOfMeasureNameField");
        setupBinder();

        addIngredient = new Button(new Icon(VaadinIcon.PLUS));
        addIngredient.setId("AddIngredientButton");
        addIngredient.setVisible(false);
        addIngredient.addClickListener(e -> {
            editor.createEditor(null);
            addIngredient.setVisible(false);
        });
        deleteIngredient = new Button(new Icon(VaadinIcon.MINUS));
        deleteIngredient.setId("DeleteIngredientButton");
        deleteIngredient.addClickListener(e-> {
            removeAll();
            editor.deleteIngredient(recipeIngredientDTO);
        });
        ingredientName.addValueChangeListener(event -> {
            fireEvent(new RecipeChangedEvent(this));
            unitOfMeasureName.setItems(ingredientService.getAllUnitOfMeasures(event.getValue()));
            addIngredient.setVisible(true);
            deleteIngredient.setVisible(true);
        });
        quantity.addValueChangeListener(event -> {
            fireEvent(new RecipeChangedEvent(this));
            addIngredient.setVisible(true);
            deleteIngredient.setVisible(true);
        });
        unitOfMeasureName.addValueChangeListener(event -> {
            fireEvent(new RecipeChangedEvent(this));
            addIngredient.setVisible(true);
            deleteIngredient.setVisible(true);
        });
        HorizontalLayout row = new HorizontalLayout(ingredientName, quantity, unitOfMeasureName, addIngredient, deleteIngredient);
        add(row);
    }

    private void setupBinder() {
        binder.forField(ingredientName).bind(RecipeIngredientDTO::getIngredientName, RecipeIngredientDTO::setIngredientName);
        binder.forField(quantity)
                .withValidator(new RegexpValidator("Не более 5 цифр и не более 2-х десятичных знаков, положительное число", "([0-9]{1,5})?([.,][0-9]{1,2})?"))
                .bind(RecipeIngredientDTO::getQuantity, RecipeIngredientDTO::setQuantity);
        binder.forField(unitOfMeasureName).bind(RecipeIngredientDTO::getUnitOfMeasureShortName, RecipeIngredientDTO::setUnitOfMeasureShortName);
    }

    @Override
    public void setValue(RecipeIngredientDTO recipeIngredientDTO) {
        binder.setBean(recipeIngredientDTO);
        if (recipeIngredientDTO!=null) {
            fieldSupport.setValue(recipeIngredientDTO);
            ingredientName.setValue(recipeIngredientDTO.getIngredientName());
            quantity.setValue(recipeIngredientDTO.getQuantity().replace('.',','));
            unitOfMeasureName.setValue(recipeIngredientDTO.getUnitOfMeasureShortName());
        }
    }

    void setRecipeIngredients() {
        if (form.getDTO()!=null) {
            Set<RecipeIngredientDTO> recipeIngredients = form.getDTO().getRecipeIngredients();
            if (recipeIngredients==null) {
                recipeIngredients = new HashSet<>();
            }
            if (recipeIngredientDTO==null) {
                recipeIngredientDTO = new RecipeIngredientDTO();
            }
            recipeIngredientDTO.setIngredientName(ingredientName.getValue());
            recipeIngredientDTO.setQuantity(quantity.getValue().replace(',','.'));
            recipeIngredientDTO.setUnitOfMeasureShortName(unitOfMeasureName.getValue());
            recipeIngredients.add(recipeIngredientDTO);
            form.getDTO().setRecipeIngredients(recipeIngredients);
        }
    }

    @Override
    public RecipeIngredientDTO getValue() {
        return fieldSupport.getValue();
    }

    @Override
    public Registration addValueChangeListener(ValueChangeListener<? super AbstractField.ComponentValueChangeEvent<RecipeIngredientEditor, RecipeIngredientDTO>> valueChangeListener) {
        return fieldSupport.addValueChangeListener(valueChangeListener);
    }

    public Registration addRecipeChangedEvent(ComponentEventListener<RecipeChangedEvent> listener) {
        return addListener(RecipeChangedEvent.class, listener);
    }

    public Binder<RecipeIngredientDTO> getBinder() {
        return binder;
    }

    public RecipeIngredientDTO getRecipeIngredientDTO() {
        return recipeIngredientDTO;
    }

    public Button getAddIngredient() {
        return addIngredient;
    }

    public Button getDeleteIngredient() {
        return deleteIngredient;
    }

    public ComboBox<String> getIngredientName() {
        return ingredientName;
    }

    public ComboBox<String> getUnitOfMeasureName() {
        return unitOfMeasureName;
    }

    public TextField getQuantity() {
        return quantity;
    }
}
