package by.weekmenu.adminka.ui.views.currency;

import by.weekmenu.adminka.DTO.CurrencyDTO;
import by.weekmenu.adminka.service.CurrencyService;
import by.weekmenu.adminka.ui.components.FormButtonsBar;
import by.weekmenu.adminka.ui.views.CrudForm;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.spring.annotation.SpringComponent;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CurrencyForm extends VerticalLayout implements CrudForm<CurrencyDTO, Integer> {

    private final TextField name;
    private final TextField code;

    private final CurrencyService currencyService;
    private final FormButtonsBar buttons;
    private CurrencyDTO currencyDTO;

    public CurrencyForm(CurrencyService currencyService) {
        this.currencyService = currencyService;
        setSizeFull();
        setId("CurrencyForm");
        name = new TextField("Валюта");
        name.setId("CurrencyNameField");
        name.focus();
        name.setWidth("100%");
        name.setRequired(true);
        name.setMaxLength(255);

        code = new TextField("Код");
        code.setId("CurrencyCodeField");
        code.setWidth("100%");
        code.setRequired(true);
        code.setMaxLength(3);

        buttons = new FormButtonsBar();
        add(name, code, buttons);
    }

    @Override
    public FormButtonsBar getButtons() {
        return buttons;
    }

    @Override
    public void setBinder(Binder<CurrencyDTO> binder, CurrencyDTO entity) {
        this.currencyDTO = entity;
        binder.forField(name)
                .asRequired("Поле не может быть пустым")
                .withValidator(field -> field.trim().length() > 0, "Поле не может быть пустым")
                .withValidator(field -> field.length() <= 255, "Не более 255 символов")
                .withValidator(field -> {
                    if (name.getValue().equals(currencyDTO.getName())) {
                        return true;
                    }else {
                       return currencyService.checkUniqueName(field) == 0;
                    }
                }, "Данное название уже существует")
                .bind(CurrencyDTO::getName, CurrencyDTO::setName);
        binder.forField(code)
                .asRequired("Поле должно содержать 3 символа")
                .withValidator(field -> field.length() == 3, "Поле должно содержать 3 символа")
                .withValidator(field -> {
                    if (code.getValue().equals(currencyDTO.getCode())) {
                        return true;
                    }else {
                        return currencyService.checkUniqueCode(field) == 0;
                    }
                }, "Данный код уже существует")
                .bind(CurrencyDTO::getCode, CurrencyDTO::setCode);
    }

    @Override
    public CurrencyDTO getDTO() {
        return currencyDTO;
    }

    @Override
    public String getChangedDTOName() {
        return currencyDTO.getName();
    }

    @Override
    public Integer getDTOId() {
        return currencyDTO.getId();
    }
}
