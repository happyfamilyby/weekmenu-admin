package by.weekmenu.adminka.ui.views.unitOfMeasure;

import by.weekmenu.adminka.DTO.UnitOfMeasureDTO;
import by.weekmenu.adminka.service.UnitOfMeasureService;
import by.weekmenu.adminka.ui.components.FormButtonsBar;
import by.weekmenu.adminka.ui.views.CrudForm;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.spring.annotation.SpringComponent;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class UnitOfMeasureForm extends VerticalLayout implements CrudForm<UnitOfMeasureDTO, Long> {

    private final TextField fullName;
    private final TextField shortName;

    private final FormButtonsBar buttons;
    private UnitOfMeasureDTO unitOfMeasureDTO;
    private final UnitOfMeasureService unitOfMeasureService;

    public UnitOfMeasureForm(UnitOfMeasureService unitOfMeasureService) {
        this.unitOfMeasureService = unitOfMeasureService;
        setSizeFull();
        setId("UnitOfMeasureForm");

        fullName = new TextField("Полное название");
        fullName.focus();
        fullName.setWidth("100%");
        fullName.setId("FullNameField");

        shortName = new TextField("Сокращённое название");
        shortName.setWidth("100%");
        shortName.setId("ShortNameField");

        buttons = new FormButtonsBar();
        add(fullName, shortName, buttons);
    }

    @Override
    public FormButtonsBar getButtons() {
        return buttons;
    }

    @Override
    public void setBinder(Binder<UnitOfMeasureDTO> binder, UnitOfMeasureDTO entity) {
        this.unitOfMeasureDTO = entity;
        binder.forField(shortName)
                .asRequired("Поле не может быть пустым")
                .withValidator(field -> field.trim().length()>0, "Поле не может быть пустым")
                .withValidator(field -> field.length()<=255, "Не более 255 символов")
                .withValidator(field-> {
                        if (shortName.getValue().equals(unitOfMeasureDTO.getShortName())) {
                            return true;
                        } else {
                            return unitOfMeasureService.checkUniqueShortName(field)==0;
                        }
                    }, "Данное сокращенное название уже существует")
                .bind(UnitOfMeasureDTO::getShortName, UnitOfMeasureDTO::setShortName);
        binder.forField(fullName)
                .asRequired("Поле не может быть пустым")
                .withValidator(field -> field.trim().length()>0, "Поле не может быть пустым")
                .withValidator(field -> field.length()<=255, "Не более 255 символов")
                .withValidator(field-> {
                        if ((fullName.getValue().equals(unitOfMeasureDTO.getFullName()))) {
                            return true;
                        } else {
                            return unitOfMeasureService.checkUniqueFullName(field)==0;
                        }
                    },"Данное полное название уже существует")
                .bind(UnitOfMeasureDTO::getFullName, UnitOfMeasureDTO::setFullName);
    }

    @Override
    public UnitOfMeasureDTO getDTO() {
        return unitOfMeasureDTO;
    }

    @Override
    public String getChangedDTOName() {
        return unitOfMeasureDTO.getFullName();
    }

    @Override
    public Long getDTOId() {
        return unitOfMeasureDTO.getId();
    }
}
