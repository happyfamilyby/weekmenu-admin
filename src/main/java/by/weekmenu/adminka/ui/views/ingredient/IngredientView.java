package by.weekmenu.adminka.ui.views.ingredient;

import by.weekmenu.adminka.DTO.IngredientDTO;
import by.weekmenu.adminka.DTO.IngredientPriceDTO;
import by.weekmenu.adminka.service.IngredientService;
import by.weekmenu.adminka.ui.MainView;
import by.weekmenu.adminka.ui.util.AppConsts;
import by.weekmenu.adminka.ui.util.Converters;
import by.weekmenu.adminka.ui.views.CrudForm;
import by.weekmenu.adminka.ui.views.CrudView;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.Set;

@Route(value = AppConsts.PAGE_INGREDIENT, layout = MainView.class)
@PageTitle(AppConsts.TITLE_INGREDIENT)
public class IngredientView extends CrudView<IngredientDTO, Long> {

    private final IngredientService serviceClient;
    private final Binder<IngredientDTO> binder;

    @Autowired
    public IngredientView(CrudForm<IngredientDTO, Long> form, IngredientService serviceClient) {
        super(form, serviceClient, null);
        this.serviceClient = serviceClient;
        binder = new Binder<>(IngredientDTO.class);
        getSearchBar().getSearchField().setVisible(false);
    }

    @Override
    public Binder<IngredientDTO> getBinder() {
        return binder;
    }

    @Override
    protected void setupGrid() {
        getGrid().addColumn(IngredientDTO::getName).setHeader("Ингредиент");
        getGrid().addColumn(ingredientDTO ->
                    String.join(" / ",
                            Converters.convertBigDecimalToString(ingredientDTO.getCalories()),
                            Converters.convertBigDecimalToString(ingredientDTO.getProteins()),
                            Converters.convertBigDecimalToString(ingredientDTO.getFats()),
                            Converters.convertBigDecimalToString(ingredientDTO.getCarbs())
        )).setHeader("К/Б/Ж/У");
    }

    @Override
    public void save() {
        IngredientDTO ingredientDTO = getForm().getDTO();
        if (ingredientDTO.getIngredientUOMS()!=null) {
            ingredientDTO.setUnitOfMeasureEquivalent(ingredientDTO.convertListToMap(ingredientDTO.getIngredientUOMS()));
        }
        if (ingredientDTO.getIngredientPrices()!=null) {
            Set<IngredientPriceDTO> ingredientPrices = ingredientDTO.getIngredientPrices();
            for (IngredientPriceDTO temp : ingredientPrices) {
                if (temp.getPriceValueString()!=null) {
                    temp.setPriceValue(new BigDecimal(temp.getPriceValueString().replace(',','.')));
                }
                if (temp.getQuantityString()!=null) {
                    temp.setQuantity(new BigDecimal(temp.getQuantityString().replace(',','.')));
                }
            }
        }
        boolean isValid = getBinder().writeBeanIfValid(ingredientDTO);
        ingredientDTO.setCalories(new BigDecimal(ingredientDTO.getCaloriesString().replace(',','.')));
        ingredientDTO.setFats(new BigDecimal(ingredientDTO.getFatsString().replace(',','.')));
        ingredientDTO.setProteins(new BigDecimal(ingredientDTO.getProteinsString().replace(',','.')));
        ingredientDTO.setCarbs(new BigDecimal(ingredientDTO.getCarbsString().replace(',','.')));
        if(isValid) {
            if (ingredientDTO.getId() == null) {
                try {
                    IngredientDTO savedEntity = serviceClient.createDTO(ingredientDTO);
                    if (savedEntity != null) {
                        Notification.show(ingredientDTO.getName() + " сохранён");
                        closeUpdate();
                    }
                } catch (Exception e) {
                    errorNotification(e);
                }
            } else {
                try{
                    serviceClient.updateDTO(ingredientDTO);
                    Notification.show(getForm().getChangedDTOName() + " обновлён");
                    closeUpdate();
                } catch (Exception e) {
                    errorNotification(e);
                }
            }
        }
    }
}
