package by.weekmenu.adminka.ui.views.region;

import by.weekmenu.adminka.DTO.RegionDTO;
import by.weekmenu.adminka.service.RegionService;
import by.weekmenu.adminka.ui.MainView;
import by.weekmenu.adminka.ui.util.AppConsts;
import by.weekmenu.adminka.ui.views.CrudForm;
import by.weekmenu.adminka.ui.views.CrudView;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;

@Route(value = AppConsts.PAGE_REGION, layout = MainView.class)
@PageTitle(AppConsts.TITLE_REGION)
public class RegionView extends CrudView<RegionDTO, Long> {

    private final Binder<RegionDTO> binder;

    @Autowired
    public RegionView(CrudForm<RegionDTO, Long> form, RegionService serviceClient) {
        super(form, serviceClient, null);
        binder = new Binder<>(RegionDTO.class);
        if(serviceClient.getAllDTOs()!=null) {
            getGrid().setItems(serviceClient.getAllDTOs());
        }
        getSearchBar().getSearchField().setVisible(false);
    }

    @Override
    public Binder<RegionDTO> getBinder() {
        return binder;
    }

    @Override
    protected void setupGrid() {

        getGrid().addColumn(RegionDTO::getCountryName).setHeader("Страна");
        getGrid().addColumn(RegionDTO::getName).setHeader("Регион");
    }
}
