package by.weekmenu.adminka.ui.views.recipeCategory;

import by.weekmenu.adminka.DTO.RecipeCategoryDTO;
import by.weekmenu.adminka.service.RecipeCategoryService;
import by.weekmenu.adminka.ui.components.FormButtonsBar;
import by.weekmenu.adminka.ui.views.CrudForm;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.spring.annotation.SpringComponent;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class RecipeCategoryForm extends VerticalLayout implements CrudForm<RecipeCategoryDTO, Long> {

    private final TextField name;
    private final FormButtonsBar buttons;
    private RecipeCategoryDTO recipeCategoryDTO;
    private final RecipeCategoryService recipeCategoryService;

    public RecipeCategoryForm(RecipeCategoryService recipeCategoryService) {
        this.recipeCategoryService = recipeCategoryService;
        setSizeFull();
        setId("RecipeCategoryForm");
        name = new TextField("Категория рецепта");
        name.focus();
        name.setWidth("100%");
        name.setId("RecipeCategoryNameField");

        buttons = new FormButtonsBar();
        add(name, buttons);
    }

    @Override
    public FormButtonsBar getButtons() {
        return buttons;
    }

    @Override
    public void setBinder(Binder<RecipeCategoryDTO> binder, RecipeCategoryDTO entity) {
        this.recipeCategoryDTO = entity;
        binder.forField(name)
                .asRequired("Поле не может быть пустым")
                .withValidator(field -> field.trim().length() > 0, "Поле не может быть пустым")
                .withValidator(field -> field.length() <= 255, "Не более 255 символов")
                .withValidator(field -> {
                    if (name.getValue().equals(recipeCategoryDTO.getName())) {
                        return true;
                    } else {
                        return recipeCategoryService.checkRecipeCategoryUniqueName(field) == 0;
                    }
                }, "Данное название категории рецепта уже внесено в базу")
                .bind(RecipeCategoryDTO::getName, RecipeCategoryDTO::setName);
    }

    @Override
    public RecipeCategoryDTO getDTO() {
        return recipeCategoryDTO;
    }

    @Override
    public String getChangedDTOName() {
        return recipeCategoryDTO.getName();
    }

    @Override
    public Long getDTOId() {
        return recipeCategoryDTO.getId();
    }
}
