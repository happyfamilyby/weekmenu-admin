package by.weekmenu.adminka.ui.views.ingredient;

import by.weekmenu.adminka.DTO.IngredientDTO;
import by.weekmenu.adminka.DTO.UnitOfMeasureDTO;
import by.weekmenu.adminka.service.UnitOfMeasureService;
import by.weekmenu.adminka.ui.Events.DeleteUOMRowEvent;
import by.weekmenu.adminka.ui.Events.EquivalentChangedEvent;
import by.weekmenu.adminka.ui.Events.FullNameChangedEvent;
import by.weekmenu.adminka.ui.beans.IngredientUOM;
import by.weekmenu.adminka.ui.views.CrudForm;
import com.vaadin.flow.component.AbstractField;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.HasValueAndElement;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.internal.AbstractFieldSupport;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationResult;
import com.vaadin.flow.data.binder.ValueContext;
import com.vaadin.flow.data.validator.RegexpValidator;
import com.vaadin.flow.shared.Registration;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

public class IngredientUOMEditor extends VerticalLayout implements HasValueAndElement<AbstractField.ComponentValueChangeEvent<IngredientUOMEditor, IngredientUOM>, IngredientUOM> {

    private final CrudForm<IngredientDTO, Long> form;
    private final ComboBox<String> fullName;
    private final TextField equivalent = new TextField("Экв. в гр.");
    private final Button addUOM;
    private final Button deleteUOM;

    private final Binder<IngredientUOM> binder = new Binder<>();
    private final AbstractFieldSupport<IngredientUOMEditor, IngredientUOM> fieldSupport;
    private IngredientUOM ingredientUOM;

    IngredientUOMEditor(CrudForm<IngredientDTO, Long> form,
                        UnitOfMeasureService unitOfMeasureService,
                        IngredientUOM ingredientUOM,
                        IngredientUOMsEditor ingredientUOMsEditor) {
        this.form = form;
        this.ingredientUOM = ingredientUOM;
        this.fieldSupport =  new AbstractFieldSupport<>(this, null,
                Objects::equals, c ->  {});
        setSizeFull();
        setPadding(false);
        setMargin(false);
        fullName = new ComboBox<>("Ед. измерения");
        fullName.setId("UnitOfMeasureFullNameField");
        fullName.setItems(unitOfMeasureService.getAllDTOs().stream()
                .map(UnitOfMeasureDTO::getFullName)
                .collect(Collectors.toList()));
        fullName.addValueChangeListener(e -> fireEvent(new FullNameChangedEvent(this, e.getValue())));
        binder.forField(equivalent)
                .asRequired("Поле не может быть пустым")
                .withValidator(new RegexpValidator("Не более 7 цифр и не более 2-x десятичных знаков", "([0-9]{1,7})?([.,][0-9]{1,2})?"))
                .bind(IngredientUOM::getEquivalent, IngredientUOM::setEquivalent);
        equivalent.setId("EquivalentField");
        equivalent.setWidth("20%");
        equivalent.addValueChangeListener(e -> fireEvent(new EquivalentChangedEvent(this, e.getValue())));
        addUOM = new Button(new Icon(VaadinIcon.PLUS));
        addUOM.setId("AddIngredientUnitOfMeasureButton");
        addUOM.setVisible(false);
        addUOM.addClickListener(e -> {
            ingredientUOMsEditor.createEditor(null);
            addUOM.setVisible(false);
        });
        deleteUOM = new Button(new Icon(VaadinIcon.MINUS));
        deleteUOM.setId("DeleteIngredientUnitOfMeasureButton");
        deleteUOM.addClickListener(e-> fireEvent(new DeleteUOMRowEvent(this)));
        fullName.addValueChangeListener(e -> {
            fireEvent(new FullNameChangedEvent(this, e.getValue()));
            addUOM.setVisible(true);
            deleteUOM.setVisible(true);
        });
        equivalent.addValueChangeListener(e -> {
            fireEvent(new EquivalentChangedEvent(this, e.getValue()));
            addUOM.setVisible(true);
            deleteUOM.setVisible(true);
        });
        binder.forField(fullName)
                .withValidator(this::checkUOM)
                .bind(IngredientUOM::getUnitOfMeasureFullName, IngredientUOM::setUnitOfMeasureFullName);
        HorizontalLayout row = new HorizontalLayout(fullName, equivalent, addUOM, deleteUOM);
        add(row);
    }

    private ValidationResult checkUOM(String fullName, ValueContext valueContext) {
        if (form.getDTO().getIngredientUOMS() != null) {
            List<String> uomNames = form.getDTO().getIngredientUOMS().stream()
                    .map(IngredientUOM::getUnitOfMeasureFullName)
                    .collect(Collectors.toList());
            Optional<String> fullNameFound = uomNames.stream().filter(fullName::equals).findAny();
            if (fullNameFound.isPresent() && uomNames.size()>1) {
                return ValidationResult.error("Для данной единицы измерения уже существует эквивалент в граммах");
            } else {
                return ValidationResult.ok();
            }
        } else {
            return ValidationResult.ok();
        }
    }

    void setIngredientUOMs() {
        if (form.getDTO()!=null) {
            List<IngredientUOM> list = form.getDTO().getIngredientUOMS();
            if (list==null) {
                list = new ArrayList<>();
            }
            if (ingredientUOM == null) {
                ingredientUOM = new IngredientUOM();
            }
                ingredientUOM.setUnitOfMeasureFullName(fullName.getValue());
                ingredientUOM.setEquivalent(equivalent.getValue().replace(',','.'));
                list.add(ingredientUOM);
                form.getDTO().setIngredientUOMS(list);
        }
    }

    @Override
    public void setValue(IngredientUOM ingredientUOM) {
        if (ingredientUOM!=null) {
            fullName.setValue(ingredientUOM.getUnitOfMeasureFullName());
            equivalent.setValue(ingredientUOM.getEquivalent().replace('.',','));
            if (fullName.getValue().equals("Грамм")) {
                fullName.setReadOnly(true);
                equivalent.setReadOnly(true);
            }
        }
        binder.setBean(ingredientUOM);
    }

    @Override
    public IngredientUOM getValue() {
            IngredientUOM ingredientUOM = new IngredientUOM();
            ingredientUOM.setUnitOfMeasureFullName(fullName.getValue());
            ingredientUOM.setEquivalent(equivalent.getValue());
            return ingredientUOM;
    }

    @Override
    public Registration addValueChangeListener(ValueChangeListener<? super AbstractField.ComponentValueChangeEvent<IngredientUOMEditor, IngredientUOM>> valueChangeListener) {
        return fieldSupport.addValueChangeListener(valueChangeListener);
    }

    Registration addFullNameChangeListener(ComponentEventListener<FullNameChangedEvent> listener) {
        return addListener(FullNameChangedEvent.class, listener);
    }

    Registration addEquivalentChangeListener(ComponentEventListener<EquivalentChangedEvent> listener) {
        return addListener(EquivalentChangedEvent.class, listener);
    }

    Registration addDeleteListener(ComponentEventListener<DeleteUOMRowEvent> listener) {
        return addListener(DeleteUOMRowEvent.class, listener);
    }

    IngredientUOM getIngredientUOM() {
        return ingredientUOM;
    }

    Binder<IngredientUOM> getBinder() {
        return binder;
    }

    Button getAddUOM() {
        return addUOM;
    }

    Button getDeleteUOM() {
        return deleteUOM;
    }

    String getFullNameField() {
        return fullName.getValue();
    }

    String getEquivalentField() {
        return equivalent.getValue();
    }
}
