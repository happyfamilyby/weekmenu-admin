package by.weekmenu.adminka.ui.views.country;

import by.weekmenu.adminka.DTO.CountryDTO;
import by.weekmenu.adminka.DTO.CurrencyDTO;
import by.weekmenu.adminka.service.CountryService;
import by.weekmenu.adminka.service.CurrencyService;
import by.weekmenu.adminka.ui.components.FormButtonsBar;
import by.weekmenu.adminka.ui.views.CrudForm;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.spring.annotation.SpringComponent;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

import java.util.stream.Collectors;

@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CountryForm extends VerticalLayout implements CrudForm<CountryDTO, Long> {

    private final TextField name;
    private final TextField code;
    private final ComboBox<String> currency;

    private final FormButtonsBar buttons;
    private CountryDTO countryDTO;
    private final CountryService countryService;

    public CountryForm(CountryService countryService, CurrencyService currencyService) {
        this.countryService = countryService;
        setSizeFull();
        setId("CountryForm");
        name = new TextField("Страна");
        name.focus();
        name.setWidth("100%");
        name.setId("NameField");

        code = new TextField("Код");
        code.setWidth("100%");
        code.setId("CodeField");

        currency = new ComboBox<>("Валюта");
        currency.setItems(currencyService.getAllDTOs().stream()
                .map(CurrencyDTO::getCode)
                .collect(Collectors.toList()));
        currency.setWidth("100%");
        currency.setId("CurrencyField");

        buttons = new FormButtonsBar();
        add(name, code, currency, buttons);
    }
    @Override
    public FormButtonsBar getButtons() {
        return buttons;
    }

    @Override
    public void setBinder(Binder<CountryDTO> binder, CountryDTO entity) {
        this.countryDTO = entity;
        binder.forField(name)
                .asRequired("Поле не может быть пустым")
                .withValidator(field -> field.trim().length()>0, "Поле не может быть пустым")
                .withValidator(field -> field.length()<=255, "Не более 255 символов")
                .withValidator(field -> {
                    if (name.getValue().equals(countryDTO.getName())) {
                        return true;
                    } else {
                        return countryService.checkUniqueName(field)==0;
                    }
                }, "Данное название страны уже внесено в базу")
                .bind(CountryDTO::getName, CountryDTO::setName);
        binder.forField(code)
                .asRequired("Поле не может быть пустым")
                .withValidator(field-> field.length()==2, "Длина должна быть равна 2-м символам")
                .withValidator(field-> {
                    if (code.getValue().equals(countryDTO.getAlphaCode2())) {
                        return true;
                    } else {
                        return countryService.checkUniqueAlphaCode2(field) == 0;
                    }
                }, "Данный код уже внесен в базу")
                .bind(CountryDTO::getAlphaCode2, CountryDTO::setAlphaCode2UpperCase);
        binder.forField(currency)
                .asRequired("Поле не может быть пустым")
                .bind(CountryDTO::getCurrencyCode, CountryDTO::setCurrencyCode);
    }

    @Override
    public CountryDTO getDTO() {
        return countryDTO;
    }

    @Override
    public String getChangedDTOName() {
        return countryDTO.getName();
    }

    @Override
    public Long getDTOId() {
        return countryDTO.getId();
    }
}
