package by.weekmenu.adminka.ui.views.ingredient;

import by.weekmenu.adminka.DTO.IngredientDTO;
import by.weekmenu.adminka.service.IngredientService;
import by.weekmenu.adminka.service.RegionService;
import by.weekmenu.adminka.service.UnitOfMeasureService;
import by.weekmenu.adminka.ui.beans.IngredientUOM;
import by.weekmenu.adminka.ui.components.FormButtonsBar;
import by.weekmenu.adminka.ui.views.CrudForm;
import com.vaadin.flow.component.html.H5;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.validator.RegexpValidator;
import com.vaadin.flow.data.validator.StringLengthValidator;
import com.vaadin.flow.spring.annotation.SpringComponent;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

import java.util.ArrayList;
import java.util.List;

@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class IngredientForm extends VerticalLayout implements CrudForm<IngredientDTO, Long> {

    private final TextField name;
    private final TextField calories;
    private final TextField carbs;
    private final TextField fats;
    private final TextField proteins;
    private final IngredientUOMsEditor uoMsEditor;
    private final IngredientPricesEditor ingredientPricesEditor;

    private final FormButtonsBar buttons;
    private IngredientDTO ingredientDTO;
    private final IngredientService ingredientService;

    public IngredientForm(IngredientService ingredientService, UnitOfMeasureService unitOfMeasureService, RegionService regionService) {
        setSizeFull();
        setId("IngredientForm");
        this.ingredientService = ingredientService;
        name = new TextField("Ингредиент");
        name.focus();
        name.setWidth("50%");
        name.setId("NameField");
        setAlignSelf(Alignment.CENTER, name);
        calories = new TextField("Ккал");
        calories.setId("CaloriesField");
        calories.setWidth("20%");
        carbs = new TextField("Углеводы");
        carbs.setId("CarbsField");
        carbs.setWidth("20%");
        fats = new TextField("Жиры");
        fats.setId("FatsField");
        fats.setWidth("20%");
        proteins = new TextField("Белки");
        proteins.setId("ProteinsField");
        proteins.setWidth("20%");
        HorizontalLayout layout = new HorizontalLayout(calories, proteins, fats, carbs);
        layout.setWidthFull();

        uoMsEditor = new IngredientUOMsEditor(this, unitOfMeasureService);
        ingredientPricesEditor = new IngredientPricesEditor(regionService, unitOfMeasureService,this);

        buttons = new FormButtonsBar();
        add(name, layout, new H5("Единицы измерения"), uoMsEditor, new H5("Цены"),
                ingredientPricesEditor, buttons);
    }

    @Override
    public FormButtonsBar getButtons() {
        return buttons;
    }

    @Override
    public void setBinder(Binder<IngredientDTO> binder, IngredientDTO entity) {
        this.ingredientDTO = entity;
        if (ingredientDTO!=null && ingredientDTO.getId()!=null) {
            ingredientDTO.setIngredientUOMS(entity.convertMapToList(entity.getUnitOfMeasureEquivalent()));
            ingredientDTO.setIngredientPrices(entity.getIngredientPrices());
            entity.setCaloriesString(String.valueOf(entity.getCalories()).replace('.',','));
            entity.setCarbsString(String.valueOf(entity.getCarbs()).replace('.',','));
            entity.setProteinsString(String.valueOf(entity.getProteins()).replace('.',','));
            entity.setFatsString(String.valueOf(entity.getFats()).replace('.',','));
        } else {
            if (ingredientDTO != null) {
                IngredientUOM gram = new IngredientUOM();
                gram.setUnitOfMeasureFullName("Грамм");
                gram.setEquivalent("1");
                List<IngredientUOM> gramList = new ArrayList<>();
                gramList.add(gram);
                ingredientDTO.setIngredientUOMS(gramList);
                uoMsEditor.createEditor(gram);
            } else {
                uoMsEditor.createEditor(null);
            }
            ingredientPricesEditor.createEditor(null);
        }

        binder.forField(calories)
                .asRequired("Поле не может быть пустым")
                .withValidator(new RegexpValidator("Не более 7 цифр и не более 1 десятичного знака, положительное число", "([0-9]{1,7})?([.,][0-9]{1})?"))
                .bind(IngredientDTO::getCaloriesString, IngredientDTO::setCaloriesString);
        Binder.Binding<IngredientDTO, String> carbsValidation = binder.forField(carbs)
                .asRequired("Поле не может быть пустым")
                .withValidator(new RegexpValidator("Не более 3 цифр и не более 1 десятичного знака, положительное число", "([0-9]{1,3})?([.,][0-9]{1})?"))
                .withValidator(num -> checkSum(String.valueOf(num), proteins.getValue(), fats.getValue()), "Сумма БЖУ не более 100")
                .bind(IngredientDTO::getCarbsString, IngredientDTO::setCarbsString);
        binder.forField(fats)
                .asRequired("Поле не может быть пустым")
                .withValidator(new RegexpValidator("Не более 3 цифр и не более 1 десятичного знака, положительное число", "([0-9]{1,3})?([.,][0-9]{1})?"))
                .bind(IngredientDTO::getFatsString, IngredientDTO::setFatsString);
        binder.forField(proteins)
                .asRequired("Поле не может быть пустым")
                .withValidator(new RegexpValidator("Не более 3 цифр и не более 1 десятичного знака, положительное число", "([0-9]{1,3})?([.,][0-9]{1})?"))
                .bind(IngredientDTO::getProteinsString, IngredientDTO::setProteinsString);
        binder.forField(uoMsEditor).bind(IngredientDTO::getIngredientUOMS, null);
        binder.forField(ingredientPricesEditor).bind(IngredientDTO::getIngredientPrices, null);
        binder.forField(name)
                .asRequired("Поле не может быть пустым")
                .withValidator(field -> field.trim().length()>0, "Поле не может быть пустым")
                .withValidator(new StringLengthValidator("Не более 255 символов", 1, 255))
                .withValidator(field -> {
                    if (name.getValue().equals(ingredientDTO.getName())) {
                        return true;
                    } else {
                        return ingredientService.checkUniqueName(field)==0;
                    }
                }, "Данное название ингредиента уже внесено в базу")
                .bind(IngredientDTO::getName, IngredientDTO::setName);
        fats.addValueChangeListener(event -> revalidateCheckSum(carbsValidation));
        proteins.addValueChangeListener(event -> revalidateCheckSum(carbsValidation));
    }

    private void revalidateCheckSum(Binder.Binding<IngredientDTO, String> carbsValidation) {
        if (!StringUtils.isEmpty(proteins.getValue()) && !StringUtils.isEmpty(carbs.getValue())
                && !StringUtils.isEmpty(fats.getValue())) {
            carbsValidation.validate();
        }
    }

    private boolean checkSum(String proteins, String carbs, String fats) {
        if (!StringUtils.isEmpty(proteins) && !StringUtils.isEmpty(carbs) && !StringUtils.isEmpty(fats)) {
            return (Double.valueOf(proteins.replace(',', '.')) +
                    Double.valueOf(carbs.replace(',', '.')) +
                    Double.valueOf(fats.replace(',', '.'))) <= 100;
        } else {
            return true;
        }
    }

    @Override
    public IngredientDTO getDTO() {
        return ingredientDTO;
    }

    @Override
    public String getChangedDTOName() {
        return ingredientDTO.getName();
    }

    @Override
    public Long getDTOId() {
        return ingredientDTO.getId();
    }
}
