package by.weekmenu.adminka.ui.views.country;

import by.weekmenu.adminka.DTO.CountryDTO;
import by.weekmenu.adminka.service.CountryService;
import by.weekmenu.adminka.ui.MainView;
import by.weekmenu.adminka.ui.util.AppConsts;
import by.weekmenu.adminka.ui.views.CrudForm;
import by.weekmenu.adminka.ui.views.CrudView;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;

@Route(value = AppConsts.PAGE_COUNTRY, layout = MainView.class)
@PageTitle(AppConsts.TITLE_COUNTRY)
public class CountryView extends CrudView<CountryDTO, Long> {

    private final Binder<CountryDTO> binder;

    @Autowired
    public CountryView(CrudForm<CountryDTO, Long> form, CountryService serviceClient) {
        super(form, serviceClient, null);
        binder = new Binder<>(CountryDTO.class);
        if(serviceClient.getAllDTOs()!=null) {
            getGrid().setItems(serviceClient.getAllDTOs());
        }
        getSearchBar().getSearchField().setVisible(false);
    }

    @Override
    public Binder<CountryDTO> getBinder() {
        return binder;
    }

    @Override
    protected void setupGrid() {
        getGrid().addColumn(CountryDTO::getName).setHeader("Страна");
        getGrid().addColumn(CountryDTO::getAlphaCode2).setHeader("Код страны");
        getGrid().addColumn(CountryDTO::getCurrencyCode).setHeader("Код валюты");
    }
}
