package by.weekmenu.adminka.ui.views.recipeCategory;

import by.weekmenu.adminka.DTO.RecipeSubcategoryDTO;
import by.weekmenu.adminka.service.RecipeSubcategoryService;
import by.weekmenu.adminka.ui.MainView;
import by.weekmenu.adminka.ui.util.AppConsts;
import by.weekmenu.adminka.ui.views.CrudForm;
import by.weekmenu.adminka.ui.views.CrudView;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;

@Route(value = AppConsts.PAGE_RECIPESUBCATEGORY, layout = MainView.class)
@PageTitle(AppConsts.TITLE_RECIPESUBCATEGORY)
public class RecipeSubcategoryView extends CrudView<RecipeSubcategoryDTO, Long> {

    private final Binder<RecipeSubcategoryDTO> binder;

    @Autowired
    public RecipeSubcategoryView(CrudForm<RecipeSubcategoryDTO, Long> form, RecipeSubcategoryService serviceClient) {
        super(form, serviceClient, null);
        binder = new Binder<>(RecipeSubcategoryDTO.class);
        if (serviceClient.getAllDTOs() != null) {
            getGrid().setItems(serviceClient.getAllDTOs());
        }
        getSearchBar().getSearchField().setVisible(false);
    }

    @Override
    public Binder<RecipeSubcategoryDTO> getBinder() {
        return binder;
    }

    @Override
    protected void setupGrid() {
        getGrid().addColumn(RecipeSubcategoryDTO::getName).setHeader("Подкатегория рецепта");
    }
}
