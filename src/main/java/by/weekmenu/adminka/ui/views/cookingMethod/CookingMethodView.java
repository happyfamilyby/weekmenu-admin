package by.weekmenu.adminka.ui.views.cookingMethod;

import by.weekmenu.adminka.DTO.CookingMethodDTO;
import by.weekmenu.adminka.service.CookingMethodService;
import by.weekmenu.adminka.ui.MainView;
import by.weekmenu.adminka.ui.util.AppConsts;
import by.weekmenu.adminka.ui.views.CrudForm;
import by.weekmenu.adminka.ui.views.CrudView;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;

@Route(value = AppConsts.PAGE_COOKINGMETHODS, layout = MainView.class)
@PageTitle(AppConsts.TITLE_COOKINGMETHODS)
public class CookingMethodView extends CrudView<CookingMethodDTO, Integer> {

    private final Binder<CookingMethodDTO> binder;

    @Autowired
    public CookingMethodView(CrudForm<CookingMethodDTO, Integer> form, CookingMethodService serviceClient) {
        super(form, serviceClient, null);
        binder = new Binder<>(CookingMethodDTO.class);
        if(serviceClient.getAllDTOs()!=null) {
            getGrid().setItems(serviceClient.getAllDTOs());
        }
        getSearchBar().getSearchField().setVisible(false);
    }

    @Override
    public Binder<CookingMethodDTO> getBinder() {
        return binder;
    }

    @Override
    protected void setupGrid() {
        getGrid().addColumn(CookingMethodDTO::getName).setHeader("Способ приготовления рецепта");
    }
}
