package by.weekmenu.adminka.ui.views.recipe;

import by.weekmenu.adminka.DTO.RecipeDTO;
import by.weekmenu.adminka.DTO.RecipeIngredientDTO;
import by.weekmenu.adminka.service.IngredientService;
import by.weekmenu.adminka.ui.views.CrudForm;
import com.vaadin.flow.component.AbstractField;
import com.vaadin.flow.component.HasValueAndElement;
import com.vaadin.flow.component.internal.AbstractFieldSupport;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.shared.Registration;

import java.util.*;
import java.util.stream.Collectors;

class RecipeIngredientsEditor extends VerticalLayout implements HasValueAndElement<AbstractField.ComponentValueChangeEvent<RecipeIngredientsEditor, Set<RecipeIngredientDTO>>, Set<RecipeIngredientDTO>> {

    private final AbstractFieldSupport<RecipeIngredientsEditor, Set<RecipeIngredientDTO>> fieldSupport;

    private final IngredientService ingredientService;
    private final CrudForm<RecipeDTO, Long> form;

    public RecipeIngredientsEditor(IngredientService ingredientService,
                                   CrudForm<RecipeDTO, Long> form) {
        setSizeFull();
        setSpacing(false);
        setPadding(false);
        this.fieldSupport = new AbstractFieldSupport<>(this, Collections.emptySet(),
                Objects::equals, c ->  {});
        this.ingredientService = ingredientService;
        this.form = form;
    }

    @Override
    public void setValue(Set<RecipeIngredientDTO> recipeIngredientDTOS) {
        fieldSupport.setValue(recipeIngredientDTOS);
        removeAll();
        if (recipeIngredientDTOS!=null) {
            List<RecipeIngredientDTO> list = new ArrayList<>(recipeIngredientDTOS);
            for (int i=0; i<list.size(); i++) {
                RecipeIngredientEditor editor = createEditor(list.get(i));
                editor.getAddIngredient().setVisible(false);
                if (i==list.size()-1) {
                    editor.getAddIngredient().setVisible(true);
                }
            }
            if (recipeIngredientDTOS.size()==0) {
                createEditor(null);
            }
        }
    }

    RecipeIngredientEditor createEditor(RecipeIngredientDTO recipeIngredientDTO) {
        RecipeIngredientEditor editor = new RecipeIngredientEditor(form, ingredientService, recipeIngredientDTO, this);
        getElement().appendChild(editor.getElement());
        editor.addRecipeChangedEvent(e -> recipeChanged(e.getSource()));
        editor.setValue(recipeIngredientDTO);
        if (recipeIngredientDTO==null) {
            editor.getDeleteIngredient().setVisible(false);
        }
        return editor;
    }

    private void recipeChanged(RecipeIngredientEditor editor) {
        if(editor.getRecipeIngredientDTO()==null) {
            if (!editor.getIngredientName().isEmpty() &&
                !editor.getQuantity().isEmpty() &&
                !editor.getUnitOfMeasureName().isEmpty()) {
                if (editor.getBinder().validate().isOk()) {
                    editor.setRecipeIngredients();
                } else {
                    editor.getBinder().validate();
                }
            }
        }
    }

    @Override
    public Set<RecipeIngredientDTO> getValue() {
        return fieldSupport.getValue();
    }

    @Override
    public Registration addValueChangeListener(ValueChangeListener<? super AbstractField.ComponentValueChangeEvent<RecipeIngredientsEditor, Set<RecipeIngredientDTO>>> valueChangeListener) {
        return fieldSupport.addValueChangeListener(valueChangeListener);
    }

    public void deleteIngredient(RecipeIngredientDTO recipeIngredientDTO) {
        setValue(getValue().stream().filter(element -> element != recipeIngredientDTO).collect(Collectors.toSet()));
        form.getDTO().setRecipeIngredients(getValue());
    }
}
