package by.weekmenu.adminka.ui.views.recipe;

import by.weekmenu.adminka.DTO.CookingStepDTO;
import by.weekmenu.adminka.ui.Events.CookingStepChangedEvent;
import by.weekmenu.adminka.ui.components.ImageUpload;
import com.vaadin.flow.component.AbstractField;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.HasValueAndElement;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.internal.AbstractFieldSupport;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.validator.StringLengthValidator;
import com.vaadin.flow.shared.Registration;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

public class CookingStepEditor extends VerticalLayout implements HasValueAndElement<AbstractField.ComponentValueChangeEvent<CookingStepEditor, CookingStepDTO>, CookingStepDTO> {

    private final TextField priority;
    private final TextArea description;
    private final TextField imageLink;
    private final Button addCookingStep;
    private final Button deleteCookingStep;
    private final Binder<CookingStepDTO> binder = new Binder<>();
    private final AbstractFieldSupport<CookingStepEditor, CookingStepDTO> fieldSupport;
    private CookingStepDTO cookingStepDto;
    private final RecipeForm form;
    
    public CookingStepEditor(String uploadLocation,
                             String pathPattern,
                             RecipeForm form,
                             CookingStepDTO cookingStepDto,
                             CookingStepsEditor editor) {
        setSizeFull();
        setPadding(false);
        setMargin(false);
        setJustifyContentMode(JustifyContentMode.EVENLY);
        this.form = form;
        this.cookingStepDto = cookingStepDto;
        this.fieldSupport =  new AbstractFieldSupport<>(this, null,
                Objects::equals, c ->  {});
        priority = new TextField();
        priority.setId("PriorityField");
        priority.setWidth("5%");
        priority.addValueChangeListener(event -> fireEvent(new CookingStepChangedEvent(this)));
        description = new TextArea();
        description.setId("DescriptionField");
        description.setWidth("60%");
        description.getStyle().set("padding", "0px");
        imageLink = new TextField("Фото");

        ImageUpload imageUpload = new ImageUpload(uploadLocation, pathPattern);
        imageUpload.uploadImage("Recipes" + "/" + form.getNameFieldValue() + "/" + "Cooking_steps_pics", imageLink);
        imageUpload.setAcceptedFileTypes("image/jpeg", "image/png", "image/gif");
        HorizontalLayout upload = new HorizontalLayout(imageUpload);
        imageLink.addValueChangeListener(e -> upload.add(createImage(pathPattern)));
        upload.setVerticalComponentAlignment(Alignment.CENTER);
        addCookingStep = new Button(new Icon(VaadinIcon.PLUS));
        addCookingStep.setId("AddCookingStepButton");
        addCookingStep.addClickListener(e -> {
            if (!description.getValue().isEmpty()) {
                editor.createEditor(null);
                addCookingStep.setVisible(false);
            } else {
                Notification.show("Введите описание шага приготовления");
            }
        });
        deleteCookingStep = new Button(new Icon(VaadinIcon.MINUS));
        deleteCookingStep.setId("DeleteCookingStepButton");
        deleteCookingStep.addClickListener(e-> {
            removeAll();
            editor.deleteCookingStep(cookingStepDto);
        });
        description.addValueChangeListener(event -> {
            fireEvent(new CookingStepChangedEvent(this));
            deleteCookingStep.setVisible(true);
        });
        setupBinder();
        HorizontalLayout row = new HorizontalLayout(priority, description, upload, addCookingStep, deleteCookingStep);
        row.setWidth("950px");
        row.setDefaultVerticalComponentAlignment(Alignment.END);
        row.setJustifyContentMode(JustifyContentMode.BETWEEN);
        add(row);
    }

    private Image createImage(String pathPattern) {
        if (imageLink.getValue() != null && !StringUtils.isEmpty(imageLink.getValue())) {
            Image image = new Image(imageLink.getValue(), cookingStepDto.getPriority());
            image.setWidth("50px");
            image.setHeight("50px");
            image.addClassNames("small-image");
            return image;
        } else {
            Image imageEmpty = new Image(pathPattern + "customProduct.png", "No image");
            imageEmpty.setHeight("50px");
            imageEmpty.setWidth("50px");
            return imageEmpty;
        }
    }

    private void setupBinder() {
        binder.forField(priority)
                .asRequired("Поле не может быть пустым")
                .withValidator(field -> {
                    if (!StringUtils.isEmpty(field)) {
                        return Integer.valueOf(field) > 0;
                    } else {
                        return true;
                    }
                }, "Положительное целое число")
                .bind(CookingStepDTO::getPriority, CookingStepDTO::setPriority);
        binder.forField(description)
                .asRequired("Поле не может быть пустым")
                .withValidator(field -> field.trim().length()>0, "Поле не может быть пустым")
                .withValidator(new StringLengthValidator("Не более 2000 символов", 1, 2000))
                .bind(CookingStepDTO::getDescription, CookingStepDTO::setDescription);
        binder.forField(imageLink).bind(CookingStepDTO::getImageLink, CookingStepDTO::setImageLink);
    }


    @Override
    public void setValue(CookingStepDTO cookingStepDto) {
        if (cookingStepDto!=null) {
            fieldSupport.setValue(cookingStepDto);
            priority.setValue(cookingStepDto.getPriority());
            description.setValue(cookingStepDto.getDescription());
        }
        binder.setBean(cookingStepDto);
    }

    void setCookingSteps() {
        if (form.getDTO()!=null) {
            List<CookingStepDTO> cookingSteps = form.getDTO().getCookingSteps();
            if (cookingSteps==null) {
                cookingSteps = new ArrayList<>();
            }
            if (cookingStepDto==null) {
                cookingStepDto = new CookingStepDTO();
            }
            cookingStepDto.setPriority(priority.getValue());
            cookingStepDto.setDescription(description.getValue());
            cookingSteps.add(cookingStepDto);
            cookingSteps.sort(Comparator.comparing(CookingStepDTO::getPriority));
            form.getDTO().setCookingSteps(cookingSteps);
        }
    }

    @Override
    public CookingStepDTO getValue() {
        return fieldSupport.getValue();
    }

    @Override
    public Registration addValueChangeListener(ValueChangeListener<? super AbstractField.ComponentValueChangeEvent<CookingStepEditor, CookingStepDTO>> valueChangeListener) {
        return fieldSupport.addValueChangeListener(valueChangeListener);
    }

    public Registration addCookingStepChangedEvent(ComponentEventListener<CookingStepChangedEvent> listener) {
        return addListener(CookingStepChangedEvent.class, listener);
    }

    public Binder<CookingStepDTO> getBinder() {
        return binder;
    }

    public CookingStepDTO getCookingStepDto() {
        return cookingStepDto;
    }

    public Button getAddCookingStep() {
        return addCookingStep;
    }

    public Button getDeleteCookingStep() {
        return deleteCookingStep;
    }

    public TextArea getDescription() {
        return description;
    }
}
