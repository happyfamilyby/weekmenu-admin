package by.weekmenu.adminka.ui.util;

public class AppConsts {

    public static final String VIEWPORT = "width=device-width, minimum-scale=1, initial-scale=1, user-scalable=yes";

    public static final String PAGE_MAIN = "admin";
    public static final String TITLE_MAIN = "Admin Panel";

    public static final String PAGE_INGREDIENT = "ingredients";
    public static final String TITLE_INGREDIENT = "Ингредиенты";
    public static final String ICON_INGREDIENT = "flask";

    public static final String PAGE_DEFAULT = PAGE_INGREDIENT;

    public static final String PAGE_UOM = "unitOfMeasures";
    public static final String TITLE_UOM = "Ед. измерения";
    public static final String ICON_UOM = "scale";

    public static final String PAGE_COUNTRY = "countries";
    public static final String TITLE_COUNTRY = "Страны";
    public static final String ICON_COUNTRY = "globe";

    public static final String PAGE_CURRENCY = "currencies";
    public static final String TITLE_CURRENCY = "Валюты";
    public static final String ICON_CURRENCY = "dollar";

    public static final String PAGE_REGION = "regions";
    public static final String TITLE_REGION = "Регионы";
    public static final String ICON_REGION = "cluster";

    public static final String PAGE_RECIPE = "recipes";
    public static final String TITLE_RECIPE = "Рецепты";
    public static final String ICON_RECIPE = "clipboard-text";

    public static final String PAGE_RECYCLE_BIN = "recycleBin";
    public static final String TITLE_RECYCLE_BIN = "Корзина";
    public static final String ICON_RECYCLE_BIN = "trash";

    public static final String PAGE_RECIPECATEGORY = "recipeCategories";
    public static final String TITLE_RECIPECATEGORY = "Категории рецептов";
    public static final String ICON_RECIPECATEGORY = "file-tree-small";

    public static final String PAGE_RECIPESUBCATEGORY = "recipeSubcategories";
    public static final String TITLE_RECIPESUBCATEGORY = "Подкатегории рецептов";
    public static final String ICON_RECIPESUBCATEGORY = "file-tree-sub";

    public static final String PAGE_COOKINGMETHODS = "cookingMethods";
    public static final String TITLE_COOKINGMETHODS = "Метод приготовления рецептов";
    public static final String ICON_COOKINGMETHODS = "clock";
}
