package by.weekmenu.adminka.ui.util;

import java.math.BigDecimal;

public class Converters {

    public static String convertBigDecimalToString(BigDecimal number) {
        if (number!=null) {
            if (number.remainder(BigDecimal.ONE).equals(new BigDecimal("0.0"))) {
                return number.toBigInteger().toString();
            } else {
                return number.toString().replace('.', ',');
            }
        } else {
            return "0";
        }
    }
}
