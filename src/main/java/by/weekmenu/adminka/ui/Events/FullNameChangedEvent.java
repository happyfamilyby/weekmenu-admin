package by.weekmenu.adminka.ui.Events;

import by.weekmenu.adminka.ui.views.ingredient.IngredientUOMEditor;
import com.vaadin.flow.component.ComponentEvent;

public class FullNameChangedEvent extends ComponentEvent<IngredientUOMEditor> {

    private final String fullName;

    public FullNameChangedEvent(IngredientUOMEditor source, String fullname) {
        super(source, false);
        this.fullName = fullname;
    }

    public String getFullName() {
        return fullName;
    }
}
