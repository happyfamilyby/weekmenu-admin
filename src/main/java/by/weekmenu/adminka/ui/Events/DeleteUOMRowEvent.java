package by.weekmenu.adminka.ui.Events;

import by.weekmenu.adminka.ui.views.ingredient.IngredientUOMEditor;
import com.vaadin.flow.component.ComponentEvent;

public class DeleteUOMRowEvent extends ComponentEvent<IngredientUOMEditor> {

	public DeleteUOMRowEvent(IngredientUOMEditor component) {
		super(component, false);
	}

}