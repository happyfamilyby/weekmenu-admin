package by.weekmenu.adminka.ui.Events;

import by.weekmenu.adminka.ui.views.recipe.CookingStepEditor;
import com.vaadin.flow.component.ComponentEvent;

public class CookingStepChangedEvent extends ComponentEvent<CookingStepEditor> {

    public CookingStepChangedEvent(CookingStepEditor component) {
        super(component, false);
    }
}
