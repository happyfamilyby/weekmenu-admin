package by.weekmenu.adminka.ui.Events;

import by.weekmenu.adminka.ui.views.recipe.RecipeIngredientEditor;
import com.vaadin.flow.component.ComponentEvent;

public class RecipeChangedEvent extends ComponentEvent<RecipeIngredientEditor> {

    public RecipeChangedEvent(RecipeIngredientEditor component) {
        super(component, false);
    }
}
