package by.weekmenu.adminka.ui.beans;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class IngredientUOM {

    private String unitOfMeasureFullName;
    private String equivalent;
}
