package by.weekmenu.adminka.ui.components;


import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.shared.Registration;

public class FormButtonsBar extends HorizontalLayout {

    private final Button save;
    private final Button delete;
    private final Button cancel;

    public FormButtonsBar() {
        setSizeFull();
        save = new Button("Сохранить");
        save.setId("SaveButton");
        delete = new Button("Удалить");
        delete.setId("DeleteButton");
        cancel = new Button("Отменить");
        cancel.setId("CancelButton");
        add(save, cancel, delete);
        setJustifyContentMode(JustifyContentMode.CENTER);
        getStyle().set("margin-top", "30px");
    }
     
    public static class SaveEvent extends ComponentEvent<FormButtonsBar> {
        SaveEvent(FormButtonsBar source, boolean fromClient) {
            super(source, fromClient);
        }
    }

    public Registration addSaveListener(ComponentEventListener<SaveEvent> listener) {
        return save.addClickListener(e -> listener.onComponentEvent(new SaveEvent(this, true)));
    }

    public static class CancelEvent extends ComponentEvent<FormButtonsBar> {
        CancelEvent(FormButtonsBar source, boolean fromClient) {
            super(source, fromClient);
        }
    }

    public Registration addCancelListener(ComponentEventListener<CancelEvent> listener) {
        return cancel.addClickListener(e -> listener.onComponentEvent(new CancelEvent(this, true)));
    }

    public static class DeleteEvent extends ComponentEvent<FormButtonsBar> {
        DeleteEvent(FormButtonsBar source, boolean fromClient) {
            super(source, fromClient);
        }
    }

    public Registration addDeleteListener(ComponentEventListener<DeleteEvent> listener) {
        return delete.addClickListener(e -> listener.onComponentEvent(new DeleteEvent(this, true)));
    }
    public Button getSaveButton() {
        return save;
    }
    public Button getDeleteButton() {
        return delete;
    }
}
