package by.weekmenu.adminka.ui.components;

import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.component.upload.receivers.MultiFileMemoryBuffer;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import static by.weekmenu.adminka.ui.views.CrudView.errorNotification;

public class ImageUpload extends HorizontalLayout {

    private final String uploadLocation;
    private final String pathPattern;

    private final MultiFileMemoryBuffer buffer;
    private final Upload upload;

    public ImageUpload(String uploadLocation, String pathPattern) {
        this.uploadLocation = uploadLocation;
        this.pathPattern = pathPattern;
        buffer = new MultiFileMemoryBuffer();
        upload = new Upload(buffer);
        add(upload);
    }

    public void setAcceptedFileTypes(String... fileTypes) {
        upload.setAcceptedFileTypes(fileTypes);
    }

    public void uploadImage(String directory, TextField imageField) {
        upload.setMaxFileSize(10000000);
        upload.setMaxFiles(1);
        upload.addSucceededListener(event-> {
            try {
                byte[] buf = new byte[(int)event.getContentLength()];
                InputStream is = buffer.getInputStream(event.getFileName());
                File dir = new File(uploadLocation + directory);
                if (!dir.exists()) {
                    dir.mkdirs();
                }
                File targetFile = new File(dir.getPath() + "/" + event.getFileName());
                FileUtils.copyInputStreamToFile(is, targetFile);
                String fileName = event.getFileName();
                if(fileName.indexOf('%')>=0) {
                    fileName = fileName.replace("%", "proc");
                    File renamedFile = new File(dir.getPath() + "/" + fileName);
                    if (renamedFile.exists()) {
                        imageField.setValue(pathPattern + directory + "/" + fileName);
                    } else {
                        FileUtils.moveFile(FileUtils.getFile(targetFile), FileUtils.getFile(renamedFile));
                        imageField.setValue(pathPattern + directory + "/" + fileName);
                    }
                } else {
                    imageField.setValue(pathPattern + directory + "/" + event.getFileName());
                }
            } catch (IOException ex) {
                errorNotification(ex);
            }
        });
    }
}
