package by.weekmenu.adminka.ui.components;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

import java.util.List;
import java.util.stream.Collectors;

@Tag("CheckConnectedElementsNotification")
public class CheckConnectedElementsNotification extends Component {

    public CheckConnectedElementsNotification(String entityName, List<String> connectedElements) {
        Button button = new Button("Закрыть");
        Label label1 = new Label("Элемент '" + entityName + "' не может быть удалён до тех пор, " +
                "пока не будут удалены следующие связанные с ним элементы:");
        List<Label> labels = connectedElements.stream().map(Label::new).collect(Collectors.toList());
        VerticalLayout layout = new VerticalLayout();
        layout.add(label1);
        labels.forEach(layout::add);
        layout.add(button);
        Notification notification = new Notification(layout);
        notification.setPosition(Notification.Position.MIDDLE);
        notification.open();
        button.addClickListener(click -> notification.close());
    }
}
