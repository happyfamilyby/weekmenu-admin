package by.weekmenu.adminka.ui.components;

import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.AfterNavigationEvent;
import com.vaadin.flow.router.AfterNavigationObserver;
import com.vaadin.flow.router.RouterLink;

import java.util.ArrayList;
import java.util.List;

public class VerticalMenu extends VerticalLayout implements AfterNavigationObserver {

    private final List<RouterLink> links = new ArrayList<>();

    public VerticalMenu() {
        setWidth("15%");
        getStyle().set("margin-top", "50px");
    }

    public void initMenu(List<PageInfo> items) {
        for (PageInfo pageInfo : items) {
            RouterLink link = new RouterLink(pageInfo.getTitle(), pageInfo.getClazz());
            links.add(link);
            add(new HorizontalLayout(new Icon("vaadin", pageInfo.getIcon()), link));
        }
    }

    @Override
    public void afterNavigation(AfterNavigationEvent event) {
        String path = event.getLocation().getPath();
        links.stream().filter(routerLink -> !routerLink.getHref().equals(path))
                .forEach(routerLink -> routerLink.getStyle().set("color", "grey").set("text-decoration", "none"));
    }
}
