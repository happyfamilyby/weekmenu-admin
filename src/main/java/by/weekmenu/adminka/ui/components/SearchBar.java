package by.weekmenu.adminka.ui.components;

import by.weekmenu.adminka.ui.views.CrudView;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.textfield.TextFieldVariant;
import com.vaadin.flow.data.value.ValueChangeMode;

public class SearchBar<E, ID> extends HorizontalLayout {

    private final CrudView<E, ID> crudView;
    private final TextField search;
    private final Button addEntity;

    public SearchBar(CrudView<E, ID> crudView, Component filterComponent) {
        this.crudView = crudView;
        search = new TextField("Поиск");
        search.setValueChangeMode(ValueChangeMode.EAGER);
        search.setPlaceholder("Введите название");
        search.setClearButtonVisible(true);
        search.addValueChangeListener(e-> getCrudView().updateList(e.getValue()));
        addEntity = new Button("Добавить");
        addEntity.addThemeVariants(ButtonVariant.MATERIAL_CONTAINED);
        addEntity.setHeight("70%");
        addEntity.addClickListener(e -> onComponentEvent());
        addEntity.setId("AddButton");

        if (filterComponent!=null) {
            add(search, filterComponent, addEntity);
        } else {
            add(search, addEntity);
        }
        setWidth("100%");
        addEntity.getStyle().set("margin-left", "auto").set("margin-top", "auto");
        getStyle().set("margin-top", "0");
        search.addThemeVariants(TextFieldVariant.MATERIAL_ALWAYS_FLOAT_LABEL);
    }

    private void onComponentEvent() {
        getCrudView().getGrid().asSingleSelect().clear();
        getCrudView().getBinder().readBean(null);
        getCrudView().getForm().setBinder(getCrudView().getBinder(), getCrudView().getCrudService().createNewDTO());
        getCrudView().getForm().getButtons().getDeleteButton().setVisible(false);
        getCrudView().getDialog().open();
    }

    private CrudView<E, ID> getCrudView() {
        return crudView;
    }

    public Button getAddEntity() {
        return addEntity;
    }

    public TextField getSearchField() {
        return search;
    }
}
