package by.weekmenu.adminka.ui.components;


import by.weekmenu.adminka.ui.util.AppConsts;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.router.AfterNavigationEvent;
import com.vaadin.flow.router.AfterNavigationObserver;

import java.util.ArrayList;
import java.util.List;

public class AppNavigation extends HorizontalLayout implements AfterNavigationObserver {

    private final Tabs tabs = new Tabs();

    private final List<String> hrefs = new ArrayList<>();
    private String currentHref;
    private String defaultHref;
 
    public void init(List<PageInfo> pages, String defaultHref) {
        this.defaultHref =  defaultHref;

        for(PageInfo page: pages) {
            Tab tab = new Tab(new Icon("vaadin", page.getIcon()), new Span(page.getTitle()));
            tab.setId(page.getLink() + "Tab");
            hrefs.add(page.getLink());
            tabs.add(tab);
        }
        setSizeUndefined();
        add(tabs);
        tabs.addSelectedChangeListener(e -> navigate());
    }

    private void navigate() {
        int tabId =tabs.getSelectedIndex();
        if (tabId >= 0 && tabId < hrefs.size()) {
            String href = hrefs.get(tabId);
            if (!href.equals(currentHref)) {
                UI.getCurrent().navigate(href);
            }
        }
    }

    @Override
    public void afterNavigation(AfterNavigationEvent event) {
        String href = event.getLocation().getFirstSegment().isEmpty() ? defaultHref
                : event.getLocation().getPath();
        currentHref = href;
        if (href.equals(AppConsts.PAGE_INGREDIENT) ||
                href.equals(AppConsts.PAGE_COUNTRY) ||
                href.equals(AppConsts.PAGE_UOM) ||
                href.equals(AppConsts.PAGE_CURRENCY) ||
                href.equals(AppConsts.PAGE_REGION)) {
            tabs.setSelectedIndex(hrefs.indexOf(AppConsts.PAGE_INGREDIENT));
        } else if (href.equals(AppConsts.PAGE_RECIPE) ||
                href.equals(AppConsts.PAGE_COOKINGMETHODS) ||
                href.equals(AppConsts.PAGE_RECIPECATEGORY) ||
                href.equals(AppConsts.PAGE_RECIPESUBCATEGORY)) {
            tabs.setSelectedIndex(hrefs.indexOf(AppConsts.PAGE_RECIPE));
        } else {
            tabs.setSelectedIndex(hrefs.indexOf(AppConsts.PAGE_RECYCLE_BIN));
        }
    }
}
