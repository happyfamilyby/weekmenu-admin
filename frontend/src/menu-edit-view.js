import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@vaadin/vaadin-split-layout/src/vaadin-split-layout.js';
import '@vaadin/vaadin-grid/src/vaadin-grid.js';

class MenuEditView extends PolymerElement {

    static get template() {
        return html`
<style include="shared-styles">
                :host {
                    display: block;
                    height: 100%;
                }
            </style>
<vaadin-split-layout style="width: 100%; height: 100%;">
 <div>
  <vaadin-grid id="vaadinGrid"></vaadin-grid>
 </div>
 <div>
   Second content element 
 </div>
</vaadin-split-layout>
`;
    }

    static get is() {
        return 'menu-edit-view';
    }

    static get properties() {
        return {
            // Declare your properties here.
        };
    }
}

customElements.define(MenuEditView.is, MenuEditView);
